import pyjapc
from auxiliary import HardwareLock, HardwareBase

class BeamBlocker(HardwareBase.HardwareBase):
    """
    A class for controling the beam blocking either by the screen (CA.BTV0215) or/and by the shutter (CO.TOSL.101.UVBEAM2_Set_Pos). 
    The movement of the screen and shutter is limited by the hardware lock.
    """
    
    # THE PARENT CLASS MAKES SURE THIS IS A SINGLETON, FOR MORE DETAIL, SEE THE PARENT CLASS
    def __init__(self, japc=None, callbackFunc=None, **kwargs) -> None:
        """
        Args:
            japc (pyjapc.Japc): the japc to be used, defaults to None (if kept None, japc to use will be automatically determined)
            callbackFunc: callback function to be triggered when either screen or shutter setting change, defaults to None
        """
        super().__init__(callbackFunc=callbackFunc, **kwargs)
        # the init method will be called every time, when the __new__ method returns something --> sometimes we must make sure the atributes are set only once
        # in other words, initialisation happens whenever BeamBlocker =´BeamBlocker.BeamBlocker() is used
        
        # whenever new japc is provided, use that one
        if japc is not None:
            self.japc = japc
        
        #####################################################################################
        # IMPORTANT: to avoid reinitialisation of atributes when the singleton class is 
        #            reused, all initialisation should be enclosed in this if statement
        #####################################################################################
        if not hasattr(self, "_wasInitialized"): 
            # internal variable holding the information if the singleton was already created
            # do not move or remove!
            self._wasInitialized = True
            
            # the logic is so that when japc is not provided (japc=None), the self.japc that was set during the first (or previous) initialisation is used (Singleton implementation)
            # however, if no japc is provided at the first initialisation of the BeamBlocker, new one must be created
            if not hasattr(self, "japc"):
                print("BeamBlocker: new instance of japc was created as none was provided.")
                self.japc = pyjapc.PyJapc("SCT.USER.ALL")
        
            # these variables will be holding the information about the shutter and screen blocking once the subscription starts
            # until the subscription starts, these variables must be set to None!
            # setting them to the correct value is done in the corresponging callback trigger (_triggerShutterCallbacks or _triggerShutterCallbacks)
            self._shutterOpenedSUBSCRIPTION = None
            self._screenInSUBSCRIPTION = None
            
            # subscribe to the screen and shutter
            self.japc.subscribeParam("CA.BTV0215/Setting#screenSelect", self._triggerScreenCallbacks, timingSelectorOverride="")
            self.japc.subscribeParam("CO.TOSL.101.UVBEAM2_Set_Pos/SettingBoolean#value", self._triggerShutterCallbacks, timingSelectorOverride="")
            
            # this class makes sure the lock is synchronised with the GUI
            self.hardwareLock = HardwareLock.HardwareLock()
        
            # beam block selection, allows to select only certain instruments to be used for blocking
            # affects only the methods BlockBeamBySelection and UnblockBeamBySelection
            # True means the instrument should be used
            self.blockSelection = {
                "Screen": True,
                "Shutter": True
            }
        #####################################################################################
        # IMPORTANT: to avoid reinitialisation of atributes when the singleton class is 
        #            reused, all initialisation should be enclosed in the if statement above
        #
        #            the __init__ funtion should not continue after this comment
        #####################################################################################
        
    def _getParamScreenIn(self) -> bool:
        "Method for internal purposes only! Avoid using this unneccessarily! Returns True if screen is inserted."
        return self.japc.getParam("CA.BTV0215/Setting#screenSelect", timingSelectorOverride="")[0] == 1
    
    def _getParamShutterOpened(self) -> bool:
        "Method for internal purposes only! Avoid using this unneccessarily! Returns True if shutter is opened. "
        return not self.japc.getParam('CO.TOSL.101.UVBEAM2_Set_Pos/SettingBoolean#value', timingSelectorOverride="")
        
    def _triggerScreenCallbacks(self, parameterName, value, **kwargs):
        self._screenInSUBSCRIPTION = value[0] == 1 # value is a tupple, value[0] == 1 if the screen is In
        # trigger the rest of registered callbacks
        self._triggerCallbacks(parameterName, value, **kwargs)
    
    def _triggerShutterCallbacks(self, parameterName, value, **kwargs):
        self._shutterOpenedSUBSCRIPTION = not value
        # trigger the rest of registered callbacks
        self._triggerCallbacks(parameterName, value, **kwargs)
    
    def RetractScreen(self) -> None:
        """Method to safely retract the screen (if hardware is unlocked).
        """
        print("Trying to retract the screen!")
        if self.CheckIfScreenOut():
            print("Screen was already out!")
            return
        if self.hardwareLock.hardwareUnlocked:
            print("Screen OUT.")
            self.japc.setParam('CA.BTV0215/Setting#screenSelect', 0, timingSelectorOverride="")
        else:
            print("Cannot since the hardware lock is ON!")
        
    def InsertScreen(self) -> None:
        """Method to safely insert the screen (if hardware is unlocked).
        """
        print("Trying to insert the screen.")
        if self.CheckIfScreenIn():
            print("Screen was already in!")
            return
        if self.hardwareLock.hardwareUnlocked:
            print("Screen IN.")
            self.japc.setParam('CA.BTV0215/Setting#screenSelect', 1, timingSelectorOverride="")
        else:
            print("Cannot since the hardware lock is ON!")
            
    def CheckIfScreenIn(self) -> bool:
        """Method to check if the screen is inserted. 
        
        If japc subscription has already started, uses the internal variable to get the state.
        If japc subscription has NOT started, gets the state by japc.getParam.
        
        Returns:
            bool: Return True if the screen is inserted otherwise False
        """
        # try to use internal variable in order to avoid using japc.getParam
        if self._screenInSUBSCRIPTION is not None:
            return self._screenInSUBSCRIPTION
        # if internal variable is not set (bcs subscription has not started yet), we have to use japc.getParam
        else:
            return self._getParamScreenIn()
    
    def CheckIfScreenOut(self) -> bool:
        """Method to check if the screen is retracted.
        
        Returns:
            bool: Return True if the screen is retracted otherwise False
        """
        return not self.CheckIfScreenIn()
    
    def CloseShutter(self) -> None:
        """Method to safely close the shutter (if hardware is unlocked).
        """
        print("Trying to close the shutter!")
        if self.CheckIfShutterClosed():
            print("Shutter was already closed!")
            return
        if self.hardwareLock.hardwareUnlocked:
            print("Shutter CLOSED.")
            self.japc.setParam('CO.TOSL.101.UVBEAM2_Set_Pos/SettingBoolean#value', True, timingSelectorOverride="")
        else:
            print("Cannot since the hardware lock is ON!")
        
    def OpenShutter(self) -> None:
        """Method to safely open the shutter (if hardware is unlocked).
    
        """
        print("Trying to open the shutter.")
        if self.CheckIfShutterOpened():
            print("Shutter was already open!")
            return
        if self.hardwareLock.hardwareUnlocked:
            print("Shutter OPEN.")
            self.japc.setParam('CO.TOSL.101.UVBEAM2_Set_Pos/SettingBoolean#value', False, timingSelectorOverride="")
        else:
            print("Cannot since the hardware lock is ON!")
            
    def CheckIfShutterOpened(self) -> bool:
        """Method to check if the shutter is open.
        
        If japc subscription has already started, uses the internal variable to get the state.
        If japc subscription has NOT started, gets the state by japc.getParam.
        
        Returns:
            bool: Return True if the shutter is open otherwise False
        """
        # try to use internal variable in order to avoid using japc.getParam
        if self._shutterOpenedSUBSCRIPTION is not None:
            return self._shutterOpenedSUBSCRIPTION
        # if internal variable is not set (bcs subscription has not started yet), we have to use japc.getParam
        else:
            return self._getParamShutterOpened()
    
    def CheckIfShutterClosed(self) -> bool:
        """Method to check if the shutter is closed.
        
        Returns:
            bool: Return True if the shutter is closed otherwise False
        """
        return not self.CheckIfShutterOpened()
    
    def BlockBeam(self) -> None:
        """This method is used to block the beam. Both screen and shutter will be used.
    
        """
        self.InsertScreen()
        self.CloseShutter()
    
    def UnblockBeam(self) -> None:
        """This method is used to unblock the beam. Both screen and shutter will be used.
        """
        self.RetractScreen()
        self.OpenShutter()
            
    def CheckIfBeamBlocked(self) -> bool:
        """Method to check if the beam is blocked (either by the screen or/and by the shutter).
        
        Returns:
            bool: Return True if the beam is blocked otherwise False
        """
        return self.CheckIfScreenIn() or self.CheckIfShutterClosed()
    
    def CheckIfBeamUnblocked(self) -> bool:
        """Method to check if the beam is unblocked  (either by the screen or/and by the shutter).
        
        Returns:
            bool: Return True if the beam is unblocked otherwise False
        """
        return not self.CheckIfBeamBlocked()
    
    def BlockBeamBySelection(self) -> None:
        """This method is used to block the beam by instruments indicated in self.blockSelection.
        """
        if self.blockSelection["Screen"]:
            self.InsertScreen()
        if self.blockSelection["Shutter"]:
            self.CloseShutter()
            
    def UnblockBeamBySelection(self) -> None:
        """This method is used to unblock the beam by instruments indicated in self.blockSelection.
        """
        if self.blockSelection["Screen"]:
            self.RetractScreen()
        if self.blockSelection["Shutter"]:
            self.OpenShutter()
            
    def CheckIfBeamBlockedBySelection(self) -> bool:
        """Method to check if the beam is blocked by instruments indicated in self.blockSelection.
        
        Returns:
            bool: Return True if the beam is blocked otherwise False
        """
        if self.blockSelection["Screen"] and self.blockSelection["Shutter"]:
            return self.CheckIfScreenIn() or self.CheckIfShutterClosed()
        if self.blockSelection["Screen"]:
            return self.CheckIfScreenIn()
        if self.blockSelection["Shutter"]:
            return self.CheckIfShutterClosed()
        
        # in case we did not fit into the parameters above
        raise NotImplementedError
    
    def CheckIfBeamUnblockedBySelection(self) -> bool:
        """Method to check if the beam is unblocked by instruments indicated in self.blockSelection.
        
        Returns:
            bool: Return True if the beam is unblocked otherwise False
        """
        return not self.CheckIfBeamBlockedBySelection()
