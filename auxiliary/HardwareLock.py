import pyjapc
from auxiliary import HardwareLock, HardwareBase

class HardwareLock(HardwareBase.HardwareBase):
    # THE PARENT CLASS MAKES SURE THIS IS A SINGLETON, FOR MORE DETAIL, SEE THE PARENT CLASS
    def __init__(self, callbackFunc=None, **kwargs) -> None:
        """
        Args:
            callbackFunc: callback function to be triggered when either screen or shutter setting change, defaults to None
        """
        super().__init__(callbackFunc=callbackFunc, **kwargs)
        # the init method will be called every time, when the __new__ method returns something --> sometimes we must make sure the atributes are set only once
        
        # at the first call of __init__method, set the hardware lock to False
        # __init__ function is called whenever the __new__ method is called, e.g. when acquiring the HardwareLock instance
        if not hasattr(self, "_hardwareUnlocked"):
            self._hardwareUnlocked = False

    def SetHardwareUnlocked(self, unlocked:bool) -> None:
        """Method to set the hardware lock.
        
        Arguments:
            unlocked (bool): True if the hardware should be unlocked, otherwise False
        """
        if not isinstance(unlocked, bool):
            raise ValueError(f"unlocked must be of type bool, not {type(unlocked)}.")
        
        self._hardwareUnlocked = unlocked
        
        self._triggerCallbacks()
        
    def IsHardwareUnlocked(self) -> bool:
        """"Method to check whether the hardware is unlocked or not.
        
        Returns:
            True if hardware is unlocked, otherwise False
        """
        return self._hardwareUnlocked
    
    @property
    def hardwareUnlocked(self) -> bool:
        """This property contains the information about the hardware lock. Unlocked = True, locked = False."""
        return self.IsHardwareUnlocked()
    
    @hardwareUnlocked.setter
    def hardwareUnlocked(self, unlocked:bool) -> None:
        self.SetHardwareUnlocked(unlocked)
        