"""
This is a base class aimed to be used as the parent class for hardware control wrapper classes. 

It makes sure the children hardware control class is created as a singleton and has certain neccessary methods.
"""

class HardwareBase(object):
    def __new__(cls, *args, **kwargs):
        """This makes sure only one instance of hardware control class is created."""
        # this is pythonic implementation of the Singleton
        # in all cases (creating first instance, getting previous instance), the __init__ method will be called automatically after __new__ --> all setup is handled there
        
        # EACH CHILD OF THIS CLASS CALLS ITS OWN __NEW__ METHOD, IT IS NOT SHARED ACROSS THE CHILDREN
        if not hasattr(cls, "_instance"):
            cls._instance = object.__new__(cls)
        return cls._instance
    
    def __init__(self, callbackFunc=None):
        """
        Args:
            callbackFunc: callback function to be registered
        """
        # callback functions
        # the hasattr function makes sure this get created only once if the singleton is implemented in the child
        if not hasattr(self, "_callbacks"):
            self._callbacks = []
            
        # try to register new callbacks, do this everytime
        self.registerCallback(callbackFunc)
    
    def registerCallback(self, callbackFunc) -> None:
        """Method to register new callback."""
        
        if callbackFunc is None:
            return
        
        if callbackFunc in self._callbacks:
            return
        
        self._callbacks += [callbackFunc]
        
        # trigger the callbacks to make sure all possible widgets are updated to the current setting
        self._triggerCallbacks()
        
    def _triggerCallbacks(self, *args, **kwargs) -> None:
        """Method to trigger all registered callbacks. To be used internally."""
        for callbackFunc in self._callbacks:
            callbackFunc()