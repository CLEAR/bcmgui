# -*- coding: utf-8 -*-

"This file contains the code to read and calibrate the BCM signals"

import pyjapc

import datetime

import numpy as np

class BCMHandler(object):
    "Class to read and process BCM data"

    #Configuration
    japc         = None
    scopeName    = None
    callback     = None
    calibrations = None # [V/nC]
    turnsFactor  = 10.0 # beam charge / Calibration charge
    chargeOffset = 0    # [nC]

    avg_start = None
    avg_end   = None

    __defaultCalib = {"6dB"  : 2.085,\
                      "12dB" : 4.18, \
                      "18dB" : 8.35, \
                      "20dB" : 10.42,\
                      "26dB1": 20.97,\
                      "26dB2": 20.95,\
                      "32dB" : 41.9, \
                      "40dB" : 105.0 } #[V/nC]

    __keeperSamples = None

    #Aquired data
    timeStamps    = None # [unixtime]
    timeSeries    = None # [nC]
    currentCallib = None # [str]
    validPoints   = None
    

    def __init__(self,scopeName,japcObject,callback=None,calibrations=None):
        self.scopeName = scopeName
        if calibrations == None:
            #No argument -> Take the default
            self.calibrations = self.__defaultCalib.copy()
        elif type(calibrations)==int or type(calibrations)==float:
            #Numerical argument -> scale the default
            self.calibrations = self.__defaultCalib.copy()
            for k in self.calibrations.keys():
                self.calibrations[k] = self.calibrations[k]*calibrations
        elif type(calibrations)==map:
            #Map argument -> replace the default
            self.calibrations = calibrations.copy()
        else:
            raise TypeError("Need float/int/map datatype")

        self.setKeeperSamples(30000)
        self.setAverageRange(20,60)

        self.japc = japcObject

        #Setup the scope
        self.japc.setParam(self.scopeName+"/Sensibility#value", 20, timingSelectorOverride='')

        #Start the aquisition
        self.callback = callback
        self.japc.subscribeParam(self.scopeName+"/Acquisition", self.recieveSignal, getHeader=True, unixtime=True)

        self.pause = False
        
    def setChargeOffset(self, value:float) -> None:
        self.chargeOffset = value
        
    def getKeeperSamples(self):
        return self.__keeperSamples
    
    def setKeeperSamples(self, keeperSamples):
        #First-time initialization
        if self.__keeperSamples is None:
            self.__keeperSamples = keeperSamples

            self.timeStamps = np.zeros(keeperSamples)
            self.timeSeries = np.zeros(keeperSamples)
            
            self.validPoints = 0
            
        #Extend or shorten the data arrays
        if len(self.timeSeries) != keeperSamples:
            raise NotImplementedError()
    
    def setAverageRange(self,startSample,endSample):
        self.avg_start = startSample
        self.avg_end   = endSample
    
    def calibrateBCM(self,signal):
        "Takes a BCM signal [V] and calibrates it [nC]"
        gainSetting = self.japc.getParam("CA.BCM01GAIN/Setting#enumValue",timingSelectorOverride="")
        chargeRaw   = np.mean(signal[self.avg_start:self.avg_end])
        charge      = self.turnsFactor*(chargeRaw / self.calibrations[gainSetting]);
        
        # apply charge offset 
        charge += self.chargeOffset
        
        return charge

    def recieveSignal(self,dataName, data, header):
        "Used to japc monitor a single signal"
        
        assert data['value_units'] == 'V'
        #Get the signals in Volts
        signal = np.asarray(data['value'],'float')*data['sensitivity'] + data['offset']
            
        if not self.pause:
            #Shuffle the data one step ahead in the array and get the new data
            self.timeSeries[:-1] = self.timeSeries[1:]
            self.timeSeries[-1]  = self.calibrateBCM(signal)

            self.timeStamps[:-1] = self.timeStamps[1:]
            self.timeStamps[-1]  = header['acqStamp']

            #print(datetime.datetime.fromtimestamp(header['acqStamp']))
            if self.validPoints < self.__keeperSamples:
                self.validPoints += 1

        if self.callback is not None:
            timeAxis = np.arange(len(data['value']))*data['sampleInterval']+data['firstSampleTime'] #[ns]
            self.callback(self.scopeName,signal,timeAxis)

    def pauseOn(self):
        self.pause = True
    def pauseOff(self):
        self.pause = False

class iBPMHandler(object):
    "Class to read and process Inductive BPM data"

    #Configuration
    japc         = None
    scopeName    = None
    callback     = None
    chargeOffset = 0    # [nC]

    avg_start = None
    avg_end   = None

    __defaultCalib   = 1e3/450.0; #[nC/(V*dt)]
    callibrationValue = None;


    zeroThreshold = 20.0e-3; #[V]

    __keeperSamples = None

    #Aquired data
    timeStamps    = None # [unixtime]
    timeSeries    = None # [nC]
    currentCallib = None # [str]
    validPoints   = None
    

    def __init__(self,scopeName,japcObject,callback=None,calibration=None):
        self.scopeName = scopeName
        if calibration == None:
            self.calibrationValue = self.__defaultCalib
        else:
            self.calibrationValue = calibration

        self.setKeeperSamples(30000)
        self.setAverageRange(20,60)

        self.japc = japcObject

        #Setup the scope
        #self.japc.setParam(self.scopeName+"/Sensibility#value", 20, timingSelectorOverride='')

        #Start the aquisition
        self.callback = callback
        self.japc.subscribeParam(self.scopeName+"/SamplesFromTrigger", self.recieveSignal, getHeader=True, unixtime=True)

        self.pause = False
        
    def setChargeOffset(self, value:float) -> None:
        self.chargeOffset = value
        
    def getKeeperSamples(self):
        return self.__keeperSamples
    
    def setKeeperSamples(self, keeperSamples):
        #First-time initialization
        if self.__keeperSamples is None:
            self.__keeperSamples = keeperSamples

            self.timeStamps = np.zeros(keeperSamples)
            self.timeSeries = np.zeros(keeperSamples)
            
            self.validPoints = 0
            
        #Extend or shorten the data arrays
        if len(self.timeSeries) != keeperSamples:
            raise NotImplementedError()
    
    def setAverageRange(self,startSample,endSample):
        self.avg_start = startSample
        self.avg_end   = endSample
    
    def calibrateBPM(self,signal):
        "Takes a iBPM signal [V] and calibrates it [nC]"
        
        # drop nan values
        signal = np.array(signal)
        signal = signal[~np.isnan(signal)]
        
        posSignal = -1.0 * signal;

        #Threshold to detect zero signal
        maxSignal = posSignal.max()
        if maxSignal < self.zeroThreshold:
            charge = 0.0
            return charge

        #Find signal boundary
        idx_min = np.argmax(posSignal > maxSignal/2.0)
        idx_max = len(posSignal) - np.argmax(posSignal[::-1] > maxSignal/2.0) - 1;
        
        #Make sure it does not include first or last 10 samples
        idx_min = max(idx_min,10)
        idx_max = min(idx_max, len(posSignal)-10)

        #Define window
        window = range(idx_min-2,idx_max+2+1)
        signalWindow = posSignal[window];
        
        charge = np.trapz(signalWindow)*self.calibrationValue;

        # apply charge offset 
        charge += self.chargeOffset

        return charge

    def recieveSignal(self,dataName, data, header):
        "Used to japc monitor a single signal"
        
        assert data['samples_units'] == 'mV'
        #Get the signals in Volts
        signalSum = np.asarray(data['samples'],'float')*1e-3;
        
        # at higher frequencies, we get a 2D matrix -> make sure it is always a 2D matrix for simpler data analysis
        if signalSum.ndim < 2:
            signalSum = np.asfarray([signalSum])
            
        if not self.pause:
            for i in range(signalSum.shape[0]):
                #Shuffle the data one step ahead in the array and get the new data
                self.timeSeries[:-1] = self.timeSeries[1:]
                self.timeSeries[-1]  = self.calibrateBPM(signalSum[i])

                self.timeStamps[:-1] = self.timeStamps[1:]
                # correctly deal with the timestamps
                if signalSum.shape[0] == 1:
                    # just use the given timestamp
                    self.timeStamps[-1]  = header["acqStamp"]
                else:
                    # divide the base interval based on the number of arrays
                    self.timeStamps[-1]  = header["acqStamp"] - (1-i/signalSum.shape[0])*1.2
            
                if self.validPoints < self.__keeperSamples:
                    self.validPoints += 1

        if self.callback is not None:
            timeAxis = np.arange(len(signalSum))*250.0 #[ns] #NOTE: ASSUMED THAT THIS SCOPE IS ALWAYS 250 MHz!
            self.callback(self.scopeName,signalSum,timeAxis)

    def pauseOn(self):
        self.pause = True
    def pauseOff(self):
        self.pause = False
