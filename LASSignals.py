# -*- coding: utf-8 -*-

"This file contains the code to read the laser power meter signals"

import pyjapc
import datetime
import numpy as np

class LasPowHandler(object):
    "Class to read and process laser power meter data of type LasPEMeter"
    
    #Configuration
    japc            = None
    deviceName      = None
    callback        = None

    scaleFactor     = None

    __keeperSamples = None

    #Aquired data
    timeStamps      = None #[unixtime]
    timeSeries      = None #[uJ]
    validPoints     = None
    
    def __init__(self, deviceName, japcObject, callback=None, scaleFactor=1):
        self.deviceName = deviceName
        self.japc = japcObject

        self.scaleFactor = scaleFactor;

        self.setKeeperSamples(30000)
    
        self.callback = callback
        self.japc.subscribeParam(self.deviceName+"/Acq#value", self.recieveSignal, \
                                 getHeader=True, unixtime=True, \
                                 timingSelectorOverride="")
        
        self.pause = False

    def getKeeperSamples(self):
        return self.__keperSamples

    def setKeeperSamples(self, keeperSamples):
        if self.__keeperSamples is None:
            self.__keeperSamples = keeperSamples
            
            self.timeStamps = np.zeros(keeperSamples)
            self.timeSeries = np.zeros(keeperSamples)
            
            self.validPoints = 0

        #Extend or shortend the data arrays
        if len(self.timeSeries) != keeperSamples:
            raise(NotImplementedError())

    def recieveSignal(self, dataName, data, header):
        "Used to japc monitor a single signal"
        
        if not self.pause:
            #Shuffle the data one step ahead in the array and get the new data
            self.timeSeries[:-1] = self.timeSeries[1:]

            data_uJ = data*1e6 #[J->uJ]
            if data_uJ > 1e44:
                self.timeSeries[-1] = np.nan
            else:
                self.timeSeries[-1]  = data_uJ*self.scaleFactor

            self.timeStamps[:-1] = self.timeStamps[1:]
            self.timeStamps[-1]  = header['acqStamp']
        
            if self.validPoints < self.__keeperSamples:
                self.validPoints += 1

        if self.callback is not None:
            self.callback(self.deviceName)

    def pauseOn(self):
        self.pause = True
    def pauseOff(self):
        self.pause = True

class LasPowScopeHandler(object):
    "Class to read and process Laser power meter data reported as a voltage on a scope"

    #Configuration
    japc         = None
    scopeName    = None
    callback     = None

    instrumentRange = None
    #Mapping of the range setting of the instrument and display
    #S = 1.0;     #x.xxxe-6
    #S = 0.1;     #xxx.xe-9
    #S = 0.001;   #xx.xxe-9
    #S = 0.0001;  #x.xxxe-9
    #S = 0.00001; #xxx.xe-12
    
    #Effect of the filtering wheel
    filterFactorMap = {0 : 2.97e-9, 1 : 1.11e-9, 2 : 2.77e-10, 3 : 2.38e-11} #[readout/uJ]
    outOfRangeWarning = None;

    avg_start = None
    avg_end   = None

    __keeperSamples = None

    #Aquired data
    timeStamps    = None # [unixtime]
    timeSeries    = None # [uJ]
    validPoints   = None
    

    def __init__(self,scopeName,japcObject,callback=None,instrumentRange=0.1):
        self.scopeName = scopeName
        self.instrumentRange = instrumentRange

        self.setKeeperSamples(30000)
        self.setAverageRange(20,60)

        self.japc = japcObject

        #Setup the scope
        self.japc.setParam(self.scopeName+"/Sensibility#value", 20, timingSelectorOverride='')

        #Start the aquisition
        self.callback = callback
        self.japc.subscribeParam(self.scopeName+"/Acquisition", self.recieveSignal, getHeader=True, unixtime=True)

        self.pause = False
        self.OutOfRangeWarning = False
        
    def getKeeperSamples(self):
        return self.__keeperSamples
    
    def setKeeperSamples(self, keeperSamples):
        #First-time initialization
        if self.__keeperSamples is None:
            self.__keeperSamples = keeperSamples

            self.timeStamps = np.zeros(keeperSamples)
            self.timeSeries = np.zeros(keeperSamples)
            
            self.validPoints = 0
            
        #Extend or shorten the data arrays
        if len(self.timeSeries) != keeperSamples:
            raise NotImplementedError()
    
    def setAverageRange(self,startSample,endSample):
        self.avg_start = startSample
        self.avg_end   = endSample
    
    def calibratePower(self,signal):
        "Takes a power meter signal signal [V] and calibrates it [uJ]"
        gainSetting = self.japc.getParam("CA.BTV0125/Setting#filterSelect", timingSelectorOverride="")[0]

        if not gainSetting in self.filterFactorMap.keys():
            if not self.OutOfRangeWarning:
                print ('Filter position ', gainSetting ,'out of range')
            self.OutOfRangeWarning = True
            return np.nan
        self.OutOfRangeWarning = False

        readout     = np.mean(signal[self.avg_start:self.avg_end])/10 * 3e-6 * self.instrumentRange
        power       = readout/self.filterFactorMap[gainSetting]
        return power

    def recieveSignal(self,dataName, data, header):
        "Used to japc monitor a single signal"
        
        assert data['value_units'] == 'V'
        #Get the signals in Volts
        signal = np.asarray(data['value'],'float')*data['sensitivity'] + data['offset']
        if not self.pause:
            #Shuffle the data one step ahead in the array and get the new data
            self.timeSeries[:-1] = self.timeSeries[1:]
            self.timeSeries[-1]  = self.calibratePower(signal)

            self.timeStamps[:-1] = self.timeStamps[1:]
            self.timeStamps[-1]  = header['acqStamp']
        
            if self.validPoints < self.__keeperSamples:
                self.validPoints += 1

        if self.callback is not None:
            timeAxis = np.arange(len(data['value']))*data['sampleInterval']+data['firstSampleTime'] #[ns]
            self.callback(self.scopeName,signal,timeAxis)

    def pauseOn(self):
        self.pause = True
    def pauseOff(self):
        self.pause = False
