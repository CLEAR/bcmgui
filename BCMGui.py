#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

import PyQt5.QtWidgets
import PyQt5.QtGui
import PyQt5.QtCore

from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtCore import Qt

import pyjapc

import BCMSignals
import LASSignals
import BCMPlots

import chargeLogTool

import collections

import numpy as np

import pathlib
import os
import enum
import string
import math

import datetime 

from auxiliary import BeamBlocker, HardwareLock
from versioner import Versioner

ver = Versioner()


class BCMGui(PyQt5.QtWidgets.QMainWindow):

    BCMsignals = collections.OrderedDict([("CE.SCOPE61.CH01" , "Gun"    ), \
                                          ("CE.SCOPE61.CH02" , "Vesper" ), \
                                          ("CA.BPM0530S-SA"  , "BPM530" ), \
                                          ("CA.BPM0595S-SA"  , "BPM595" ), \
                                          ("CA.BPM0820S-SA"  , "BPM820" ), \
                                          ("CE.SCOPE61.CH03" , "THz"    )    ])
    
    # BCM calibration factors were multiplied by 10 for BPMs to have correct charge values
    
    BCMcalibs = {'BPM530':1e3/4450.0, 'BPM595':1e3/4250.0, 'BPM820':1e3/4200.0, \
                 "Gun":0.5, "THz":0.5 }

    LASERsignals = collections.OrderedDict([("CO.BOEB.301.CATLASEN2", "Laser lab"), \
                                            ("CO.BOEB.301.TRALASEN1", "CLEX")])

    clearDataDir = pathlib.Path("/clear/data")

    def __init__ (self, qApp:PyQt5.QtWidgets.QApplication):
        PyQt5.QtWidgets.QMainWindow.__init__(self)

        # PyQt5.QtWidgets.QApplication that was setup
        self.qApp = qApp

        # holds the information about turning on the dark mode
        self.darkModeToggled = False

        #General initialization
        self.japc = pyjapc.PyJapc("SCT.USER.ALL")
    
        # this makes sure that the lock is synchronised with the hardware control instances
        self.hardwareLock = HardwareLock.HardwareLock()
        # callbacks will be registered at the end of the __init__
        
        self.BeamBlocker = BeamBlocker.BeamBlocker(japc=self.japc)
        # callbacks will be registered at the end of the __init__
        
        self.signalHandlers = {}
        
        for k,v in self.BCMsignals.items():
            if v in self.BCMcalibs:
                calib = self.BCMcalibs[v]
            else:
                calib = None;

            if "BPM" in k:
                self.signalHandlers[k] = BCMSignals.iBPMHandler(k, self.japc, self.BCMsignalCallback, calibration=calib)
            else:
                self.signalHandlers[k] = BCMSignals.BCMHandler(k, self.japc, self.BCMsignalCallback, calibrations=calib)

        for k,v in self.LASERsignals.items():
            if k.startswith("CE.SCOPE"):
                self.signalHandlers[k] = LASSignals.LasPowScopeHandler(k,self.japc, self.LASERsignalCallback)
            else:
                self.signalHandlers[k] = LASSignals.LasPowHandler(k,self.japc, self.LASERsignalCallback)

        self.guncharge = 0.0 #Used for reference
        
        self.accumulatedChargeAndEnergy = {}
        for k,v in self.BCMsignals.items():
            self.accumulatedChargeAndEnergy[k] = 0.0
        for k,v in self.LASERsignals.items():
            self.accumulatedChargeAndEnergy[k] = 0.0

        self.isPaused = False

        #Build the main window
        self.setWindowTitle("BCM Gui")
        self.setAttribute(PyQt5.QtCore.Qt.WA_DeleteOnClose)
        
        # set the maximum width of the window
        maxWindowWidth = self.qApp.primaryScreen().availableGeometry().width()
        self.setMaximumWidth(maxWindowWidth)
        # by default, open GUI in maximized size
        self.showMaximized()

        self.tabsWidget = PyQt5.QtWidgets.QTabWidget(self)
        self.setCentralWidget(self.tabsWidget)

        self.tabHist = PyQt5.QtWidgets.QWidget(self)
        self.buildTabHist()
        self.tabsWidget.addTab(self.tabHist, "Charge history")
        self.tabIndexHist = 0

        self.tabRaw  = PyQt5.QtWidgets.QWidget(self)
        self.buildTabRaw()
        self.tabsWidget.addTab(self.tabRaw, "Raw signals")
        self.tabIndexRaw = 1

        self.tabCorr = PyQt5.QtWidgets.QWidget(self)
        self.buildTabCorr()
        self.tabsWidget.addTab(self.tabCorr, "Correlations")
        self.tabIndexCorr = 2

        #Handle multithreading correctly when updating the GUI
        self.update_BCMlabels_signal.connect(self.update_BCMlabels)
        self.update_LASERlabels_signal.connect(self.update_LASERlabels)
        
        self.show()

        # reister the callbacks
        self.hardwareLock.registerCallback(self.updateHardwareStatusBar)
        self.BeamBlocker.registerCallback(self._updateScreenStatus)
        self.BeamBlocker.registerCallback(self._updateShutterStatus)
        self.BeamBlocker.registerCallback(self.resetBeamBlockButton)
        
        self._buildDarkModeAction()
        self._createContextMenu()
        
        self.japc.startSubscriptions()

    def getVersionForString(self):
        versionStr = "(v"
        versionStr += str(ver.getVersion())
        
        # if git reporsitory was initialised, display basic information
        if ver.isGitReporsitory():
            versionStr += "*" if ver.reporsitoryAdjusted() else ""
            versionStr += ", b: " + str(ver.getBrancheName())
        else:
            versionStr += ", no git version tracking!"
            
        versionStr += ")"
        return versionStr

    ## HISTORY TAB ##
    def buildTabHist(self):
        vbox = PyQt5.QtWidgets.QVBoxLayout(self.tabHist)
        
        # Build the check buttons
        tracesGrid = PyQt5.QtWidgets.QGridLayout()
        vbox.addLayout(tracesGrid)
        
        # make check box for each item in BCMsignals and LASER signals
        check_box_label = PyQt5.QtWidgets.QLabel("Include traces: ")
        tracesGrid.addWidget(check_box_label,0, 0)
        self.tracesCheckBoxes = {}
        l=1
        for k,v in list(self.BCMsignals.items())+list(self.LASERsignals.items()):
            self.tracesCheckBoxes[v] = PyQt5.QtWidgets.QCheckBox(v)
            self.tracesCheckBoxes[v].setChecked(True)
            tracesGrid.addWidget(self.tracesCheckBoxes[v],0, l)
            self.tracesCheckBoxes[v].stateChanged.connect(lambda:self.tracesVisibility_refresh())
                
            l+=1
            
        vlineTraces = PyQt5.QtWidgets.QFrame()
        vlineTraces.setFrameShape(PyQt5.QtWidgets.QFrame.VLine)
        tracesGrid.addWidget(vlineTraces, 0, l, 1,1)
        
        hline = PyQt5.QtWidgets.QFrame()
        hline.setFrameShape(PyQt5.QtWidgets.QFrame.HLine)
        tracesGrid.addWidget(hline, 1,0, 1, l+3)
        
        # add offset setting
        tracesGrid.addWidget( PyQt5.QtWidgets.QLabel("Charge offset [pC]: "), 0,l+1,1,1)
        self.chargeOffsetQLineEdit = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.chargeOffsetQLineEdit.setFixedWidth(100)
        self.chargeOffset = 0
        self.chargeOffsetQLineEdit.setText( str(self.chargeOffset) )
        self.chargeOffsetQLineEdit.editingFinished.connect(self.edit_chargeOffset)
        self.chargeOffsetQLineEdit.returnPressed.connect  (self.edit_chargeOffset)
        charge_margin_validator = PyQt5.QtGui.QDoubleValidator()
        self.chargeOffsetQLineEdit.setValidator(charge_margin_validator)
        tracesGrid.addWidget(self.chargeOffsetQLineEdit,0,l+2,1,1)
        
        #initialize plot window
        self.histPlot = BCMPlots.BCMHistCanvas(list(self.BCMsignals.values()), list(self.LASERsignals.values()), \
                                               self.signalHandlers,\
                                               parent=self.tabHist, width=6,height=6, minWidth=3,minHeight=3)
        vbox.addWidget(self.histPlot)
        
        #Build the buttons
        grid = PyQt5.QtWidgets.QGridLayout()
        vbox.addLayout(grid)

        #TODO: Add here:
        # - Save to file
        
        self.currentChargeWidget = {}
        self.chargeStatsWidget   = {}
        self.chargeStats2Widget  = {}
        self.currentRatioWidget  = {}
        self.accChargeStats      = {}
        i = 0
        for k,v in self.BCMsignals.items():
            if i != 0:
                vline = PyQt5.QtWidgets.QFrame()
                vline.setFrameShape(PyQt5.QtWidgets.QFrame.VLine)
                grid.addWidget(vline, 0,3*i-1, 5,1)
                
            nameLabel = PyQt5.QtWidgets.QLabel(v, self.tabHist)
            currentFont = nameLabel.font()
            currentFont.setPointSize( int(currentFont.pointSize()*2.0) )
            nameLabel.setFont(currentFont)
            grid.addWidget(nameLabel, 0,3*i)
            
            self.currentChargeWidget[k] = PyQt5.QtWidgets.QLabel("0.0 pC", self.tabHist)
            self.currentChargeWidget[k].setFont(currentFont)
            grid.addWidget(self.currentChargeWidget[k], 0,3*i+1)

            if i != 0:
                self.currentRatioWidget[k] = PyQt5.QtWidgets.QLabel(v+" = 0.0 %", self.tabHist)
                self.currentRatioWidget[k].setFont(currentFont)
                grid.addWidget(self.currentRatioWidget[k],1,3*i+1)
            else:
                ratiosLabel = PyQt5.QtWidgets.QLabel("Ratios:", self.tabHist)
                ratiosLabel.setFont(currentFont)
                grid.addWidget( ratiosLabel, 1,0)
                
            if i == 0:
                MeanRmsLabel = PyQt5.QtWidgets.QLabel("μ±σ (PtoP):", self.tabHist)
                #MeanRmsLabel.setFont(currentFont)
                grid.addWidget( MeanRmsLabel, 2,0)
                
                RelRmsPpLabel = PyQt5.QtWidgets.QLabel("σ/μ, PtoP/μ:", self.tabHist)
                #RelRmsPpLabel.setFont(currentFont)
                grid.addWidget( RelRmsPpLabel, 3,0)
                
                self.AccChargeLabel = PyQt5.QtWidgets.QLabel("AccQ [nC]:", self.tabHist)
                grid.addWidget( self.AccChargeLabel, 4,0)
                
            self.chargeStatsWidget[k] = PyQt5.QtWidgets.QLabel("NaN ± NaN", self.tabHist)
            #self.chargeStatsWidget[k].setFont(currentFont)
            grid.addWidget( self.chargeStatsWidget[k], 2,3*i+1,1,1 )

            self.chargeStats2Widget[k] = PyQt5.QtWidgets.QLabel("NaN ± NaN", self.tabHist)
            #self.chargeStats2Widget[k].setFont(currentFont)
            grid.addWidget( self.chargeStats2Widget[k], 3,3*i+1,1,1 )
            
            self.accChargeStats[k] = PyQt5.QtWidgets.QLabel("0", self.tabHist)
            grid.addWidget( self.accChargeStats[k], 4,3*i+1,1,1 )

            i+=1
        
        
        self.currentEnergyWidget = {}
        self.energyStatsWidget   = {}
        self.energyStats2Widget  = {}
        self.qEffWidget          = {}
        self.accEnergyStats          = {}
        j = 0
        for k,v, in self.LASERsignals.items():
            if j != 0:
                vline = PyQt5.QtWidgets.QFrame()
                vline.setFrameShape(PyQt5.QtWidgets.QFrame.VLine)
                grid.addWidget(vline, 6,3*j-1, 5,1)
                
            nameLabel = PyQt5.QtWidgets.QLabel(v, self.tabHist)
            currentFont = nameLabel.font()
            currentFont.setPointSize( int(currentFont.pointSize()*2.0) )
            nameLabel.setFont(currentFont)
            grid.addWidget(nameLabel, 6,3*j )
            self.currentEnergyWidget[k] = PyQt5.QtWidgets.QLabel("0.0 μJ", self.tabHist)
            self.currentEnergyWidget[k].setFont(currentFont)
            grid.addWidget(self.currentEnergyWidget[k], 6,3*j+1)
            
            if j == 0:
                MeanRmsPPLabel_energy = PyQt5.QtWidgets.QLabel("μ±σ (PtoP):", self.tabHist)
                #MeanRmsPPLabel_energy.setFont(currentFont)
                grid.addWidget( MeanRmsPPLabel_energy, 7,0)
                
                RelRmsPpLabel_energy = PyQt5.QtWidgets.QLabel("σ/μ, PtoP/μ:", self.tabHist)
                #RelRmsPpLabel_energy.setFont(currentFont)
                grid.addWidget( RelRmsPpLabel_energy, 8,0)

                QeffLabel = PyQt5.QtWidgets.QLabel("Qeff [%]:", self.tabHist)
                grid.addWidget( QeffLabel, 9,0)
                
                self.accEnergyLabel = PyQt5.QtWidgets.QLabel("AccE [μJ]:", self.tabHist)
                grid.addWidget( self.accEnergyLabel, 10,0)

            self.energyStatsWidget[k] = PyQt5.QtWidgets.QLabel("NaN ± NaN", self.tabHist)
            #self.energyStatsWidget[k].setFont(currentFont)
            grid.addWidget( self.energyStatsWidget[k], 7,3*j+1,1,1 )

            self.energyStats2Widget[k] = PyQt5.QtWidgets.QLabel("NaN ± NaN", self.tabHist)
            #self.energyStats2Widget[k].setFont(currentFont)
            grid.addWidget( self.energyStats2Widget[k], 8,3*j+1,1,1 )

            self.qEffWidget[k] = PyQt5.QtWidgets.QLabel("NaN", self.tabHist)
            grid.addWidget( self.qEffWidget[k], 9,3*j+1,1,1 )
            
            self.accEnergyStats[k] = PyQt5.QtWidgets.QLabel("0", self.tabHist)
            grid.addWidget( self.accEnergyStats[k], 10,3*j+1,1,1 )

            j+=1
            
        self.change_accChargeEnergy_color("grey")

        hline1 = PyQt5.QtWidgets.QFrame()
        hline1.setFrameShape(PyQt5.QtWidgets.QFrame.HLine)
        grid.addWidget(hline1, 5,0, 1,3*max(i,j)-1)
        
        hline2 = PyQt5.QtWidgets.QFrame()
        hline2.setFrameShape(PyQt5.QtWidgets.QFrame.HLine)
        grid.addWidget(hline2, 11,0, 1,3*max(i,j)-1)
        
        grid2 = PyQt5.QtWidgets.QGridLayout()
        grid.addLayout(grid2, 12,0, 1,3*max(i,j)-1)
        
        grid2.addWidget( PyQt5.QtWidgets.QLabel("Samples to show:", self.tabHist), 0,0,1,1)
        self.numdisp_max = self.get_numdisp_max()
        self.numdisp     = self.numdisp_max
        self.field_numdisp = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.field_numdisp.setText( str(self.numdisp) )
        self.field_numdisp.editingFinished.connect(self.edit_numdisp)
        self.field_numdisp.returnPressed.connect  (self.edit_numdisp)
        numdisp_val = PyQt5.QtGui.QIntValidator()
        numdisp_val.setBottom(2)
        self.field_numdisp.setValidator(numdisp_val)
        grid2.addWidget(self.field_numdisp,0,1,1,1)
        
        self.pauseButton = PyQt5.QtWidgets.QPushButton("Pause Acquisition")
        self.pauseButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPlay))
        self.pauseButton.clicked.connect(self.pauseButton_click)
        grid2.addWidget(self.pauseButton, 0,2,1,2)
        
        grid2.addWidget( PyQt5.QtWidgets.QLabel("Max charge to show (0.0 for auto):", self.tabHist), 1,0,1,1)
        self.ydispBCM     = 0.0
        self.field_ydispBCM = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.field_ydispBCM.setText( str(self.ydispBCM) )
        self.field_ydispBCM.editingFinished.connect(self.edit_ydispBCM)
        self.field_ydispBCM.returnPressed.connect  (self.edit_ydispBCM)
        ydispBCM_val = PyQt5.QtGui.QDoubleValidator()
        ydispBCM_val.setBottom(0.0)
        self.field_ydispBCM.setValidator(ydispBCM_val)
        grid2.addWidget(self.field_ydispBCM,1,1,1,1)
        
        grid2.addWidget( PyQt5.QtWidgets.QLabel("Max laser energy to show (0.0 for auto):", self.tabHist), 1,2,1,1)
        self.ydispLAS   = 0.0
        self.field_ydispLAS = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.field_ydispLAS.setText( str(self.ydispLAS) )
        self.field_ydispLAS.editingFinished.connect(self.edit_ydispLAS)
        self.field_ydispLAS.returnPressed.connect  (self.edit_ydispLAS)
        ydispLAS_val = PyQt5.QtGui.QDoubleValidator()
        ydispLAS_val.setBottom(0.0)
        self.field_ydispLAS.setValidator(ydispLAS_val)
        grid2.addWidget(self.field_ydispLAS,1,3,1,1)
        
        hline3 = PyQt5.QtWidgets.QFrame()
        hline3.setFrameShape(PyQt5.QtWidgets.QFrame.HLine)
        grid.addWidget(hline3, 13,0, 1,3*max(i,j)-1)
        
        self.grid3 = PyQt5.QtWidgets.QGridLayout()
        grid.addLayout(self.grid3, 14,0, 1,3*max(i,j)-1)
        
        chargeEnergyLogLabel = PyQt5.QtWidgets.QLabel("Charge and Laser Energy Logging Tool")
        currentFont = chargeEnergyLogLabel.font()
        currentFont.setPointSize( int(currentFont.pointSize()*1.5) )
        chargeEnergyLogLabel.setFont(currentFont)
        self.grid3.addWidget(chargeEnergyLogLabel , 0,0,1,1)
        
        # get current working directory and set it as default one
        self.defaultChargeLogDir = pathlib.Path().absolute()
        # if CLEAR data directory exists and we have the write pesmission, use that instead
        if self.clearDataDir.is_dir():
            # make sure we can write in that dir
            if os.access(self.clearDataDir, os.W_OK):
                # if exists and is writable, use the clear directory
                self.defaultChargeLogDir = self.clearDataDir
        # create subdirectory for storing logs
        self.defaultChargeLogDir = self.defaultChargeLogDir.joinpath("chargeEnergyLog")
        # make sure it exists
        self.defaultChargeLogDir.mkdir(exist_ok=True)
        
        self.grid3.addWidget( PyQt5.QtWidgets.QLabel("Log file (set to empty to generate new name): "), 1,0,1,1)
        self.charge_log_file_QLineEdit = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.chargeLogFile = self._get_defaul_log_file_name(self.defaultChargeLogDir)
        self.charge_log_file_QLineEdit.setText( str(self.chargeLogFile) )
        self.charge_log_file_QLineEdit.editingFinished.connect(self.edit_chargeLogFile)
        # self.charge_log_file_QLineEdit.returnPressed.connect  (self.edit_chargeLogFile)
        self.grid3.addWidget(self.charge_log_file_QLineEdit,1,1,1,1)
        
        fileBrowserButton = PyQt5.QtWidgets.QPushButton("Browse files")
        fileBrowserButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_DirOpenIcon))
        fileBrowserButton.clicked.connect(self.fileBrowserButton_click)
        self.grid3.addWidget(fileBrowserButton, 1,2,1,1)
        
        self.CheckBox_autoUpdateLogFileName = PyQt5.QtWidgets.QCheckBox("Autoupdate file name")
        self.CheckBox_autoUpdateLogFileName.setChecked(True)
        self.grid3.addWidget(self.CheckBox_autoUpdateLogFileName, 1,3,1,1)
        
        self.chargeLogRunning = False
        self.chargeLogger = None
        self.chargeLogButton = PyQt5.QtWidgets.QPushButton("Start/continue logging")
        self.chargeLogButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPlay))
        self.chargeLogButton.clicked.connect(self.chargeLogButton_click)
        self.grid3.addWidget(self.chargeLogButton, 1,4,1,1)
        
        self.hardwareLockButton = PyQt5.QtWidgets.QPushButton("Unlock hardware")
        self.hardwareLockButton.setStyleSheet("background-color : yellow")
        # self.hardwareLockButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPlay))
        self.hardwareLockButton.clicked.connect(self.hardwareLockButton_click)
        self.grid3.addWidget(self.hardwareLockButton, 1,5,1,1)
        
        # status bar section
        self.status_bar = self.statusBar()  
        
        # add widget with the current version
        self.versionStatusBarLabel = PyQt5.QtWidgets.QLabel(self.getVersionForString())
        self.versionStatusBarLabel.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.status_bar.addPermanentWidget(self.versionStatusBarLabel)
        
        self.shutterStatusBarLabel = PyQt5.QtWidgets.QLabel("")
        self.shutterStatusBarLabel.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.status_bar.addPermanentWidget(self.shutterStatusBarLabel)
        # this will correctly set the the text of the widget
        self._updateShutterStatus()
        
        self.screenStatusBarLabel = PyQt5.QtWidgets.QLabel("")
        self.screenStatusBarLabel.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.status_bar.addPermanentWidget(self.screenStatusBarLabel)
        # this will correctly set the the text of the widget
        self._updateScreenStatus()
        
        self.loggingStatusBarLabel = PyQt5.QtWidgets.QLabel(" Logging OFF ")
        self.loggingStatusBarLabel.setStyleSheet("background-color: lightgrey; border: 1px solid black;")
        self.loggingStatusBarLabel.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.status_bar.addPermanentWidget(self.loggingStatusBarLabel)
        
        self.hardwareStatusBarLabel = PyQt5.QtWidgets.QLabel(" Hardware LOCKED ")
        self.hardwareStatusBarLabel.setStyleSheet("background-color: lightgreen; border: 1px solid black;")
        self.hardwareStatusBarLabel.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.status_bar.addPermanentWidget(self.hardwareStatusBarLabel)
        # update status bar to match the current setting
        self.updateHardwareStatusBar()
        
        # logging setup section
        self.LoggingSelectionOptions = enum.Enum('Mode', ['DefinedRuns', 'Accumulating', "HistoryCapture"], start=0)
        # DefinedRuns    -> runs for: marginShots + measureShots + marginShots
        # Accumulating   -> runs until accumulated value is recorded on given instrument
        # Manual         -> manual mode
        # HistoryCapture -> captures n last shots
        self.currentLoggingSelection = self.LoggingSelectionOptions.DefinedRuns
        self.loggingSelectionGrids = []
        self.loggingSelection = PyQt5.QtWidgets.QComboBox()
        self.loggingSelection.addItem("Define margin and measure runs")
        self.loggingSelection.addItem("Define target accumulated charge/energy")
        self.loggingSelection.addItem("Capture history shots")
        self.loggingSelection.currentIndexChanged.connect(self.loggingSelection_change)
        self.grid3.addWidget(self.loggingSelection,2,0,1,1)
        
        # initialise the sections for different logging setup options
        self._initialise_gridMarginMeasureRuns()
        self._initialise_gridTargetAccValue()
        self._initialise_HistoryCaptureMode()
        # activate (show) the default one
        self.loggingSelectionGrids[0].show()
        
        # beam block selection
        # this enum is later used to determine what option was selected
        self.beamBlockingSelectionOptions = enum.Enum('Mode', ['ScreenAndShutter', 'ScreenOnly', "ShutterOnly"], start=0)
        self.beamBlockingSelection = PyQt5.QtWidgets.QComboBox()
        self.beamBlockingSelection.addItem("Use Screen&Shutter")
        self.beamBlockingSelection.addItem("Use Screen")
        self.beamBlockingSelection.addItem("Use Shutter")
        self.beamBlockingSelection.currentIndexChanged.connect(self.beamBlockingSelection_change)
        self.grid3.addWidget(self.beamBlockingSelection,2,4,1,1)
        
        self.beamBlockButton = PyQt5.QtWidgets.QPushButton("")
        self.beamBlockButton.setStyleSheet("background-color : yellow")
        self.beamBlockButton.clicked.connect(self.beamBlockButtonClick)
        self.grid3.addWidget(self.beamBlockButton, 2,5,1,1)
        # make sure that the text on the beamBlockButton matches the current screen state
        self.resetBeamBlockButton()
        
        
    def _get_defaul_log_file_name(self, logDir):
        """This method generates a unique default log file name for each session.
        """
        defaultName = f"log_{datetime.datetime.now().strftime(r'%Y%m%d_%H%M%S')}.csv"
        
        # sessionIdentifier = 1
        # while pathlib.Path(logDir).joinpath(defaultName).is_file():
        #     defaultName = f"log_{datetime.datetime.now().strftime(r'%Y_%m_%d')}_n{sessionIdentifier}.csv"
        #     sessionIdentifier += 1
            
        return pathlib.Path(logDir).joinpath(defaultName)

    def _initialise_gridMarginMeasureRuns(self):
        # this method initialises the layout for defining the number of margin and measure runs
        grid = PyQt5.QtWidgets.QGridLayout()
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("Margin shots: "), 1,0,1,1)
        self.charge_margin_shots = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.marginShots = 0
        self.charge_margin_shots.setText( str(self.marginShots) )
        self.charge_margin_shots.editingFinished.connect(self.edit_marginShots)
        self.charge_margin_shots.returnPressed.connect  (self.edit_marginShots)
        charge_margin_validator = PyQt5.QtGui.QIntValidator()
        charge_margin_validator.setBottom(0)
        self.charge_margin_shots.setValidator(charge_margin_validator)
        grid.addWidget(self.charge_margin_shots,1,1,1,1)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("Measure shots (set 0 for indefinite): "), 1,2,1,1)
        self.charge_measure_shots = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.measureShots = 0
        self.charge_measure_shots.setText( str(self.measureShots) )
        self.charge_measure_shots.editingFinished.connect(self.edit_measureShots)
        self.charge_measure_shots.returnPressed.connect  (self.edit_measureShots)
        charge_measure_validator = PyQt5.QtGui.QIntValidator()
        charge_measure_validator.setBottom(0)
        self.charge_measure_shots.setValidator(charge_margin_validator)
        grid.addWidget(self.charge_measure_shots,1,3,1,1)
        
        # adding it as a frame widget allows to hide it when we want to defie the logging parameters in different way (see the self.loggingSelection)
        # using frame is kind of a workaround as the grid itself cannot be hidden
        frame = PyQt5.QtWidgets.QFrame()
        frame.setLayout(grid)
        frame.hide()
        self.grid3.addWidget(frame, 2,1, 1,3)
        self.loggingSelectionGrids.append(frame)
        
    def _initialise_gridTargetAccValue(self):
        # this method initialises the layout for defining the target value on a instrument
        
        grid = PyQt5.QtWidgets.QGridLayout()
        # self.grid3.addLayout(grid, 2,1, 1,4)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("Margin shots: "), 1,0,1,1)
        
        self.charge_margin_shots2 = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.charge_margin_shots2.setText( str(self.marginShots) )
        self.charge_margin_shots2.editingFinished.connect(self.edit_marginShots2)
        self.charge_margin_shots2.returnPressed.connect  (self.edit_marginShots2)
        charge_margin_validator = PyQt5.QtGui.QIntValidator()
        charge_margin_validator.setBottom(0)
        self.charge_margin_shots2.setValidator(charge_margin_validator)        
        grid.addWidget(self.charge_margin_shots2,1,1,1,1)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("Select instrument: "), 1,2,1,1)
        self.instrumentSelection = PyQt5.QtWidgets.QComboBox()
        for k,v in self.BCMsignals.items():
            self.instrumentSelection.addItem(v)
        for k,v in self.LASERsignals.items():
            self.instrumentSelection.addItem(v)
        self.targetValueLogPrimaryInstrument = self.instrumentSelection.currentText()
        self.instrumentSelection.currentIndexChanged.connect(self.instrumentSelection_change)
        grid.addWidget(self.instrumentSelection,1,3,1,1)
        
        self.targetAccumulatedValueLabel = PyQt5.QtWidgets.QLabel("Target accumulated charge [nC]: ")
        # this makes sure the label is correctly set even if there are no BCMsignals
        self.instrumentSelection_change(0)
        grid.addWidget(self.targetAccumulatedValueLabel , 1,4,1,1)
        
        self.targetAccValueQLineEdit = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.targetAccValue = 0
        self.targetAccValueQLineEdit.setText( str(self.targetAccValue) )
        self.targetAccValueQLineEdit.editingFinished.connect(self.edit_targetAccValue)
        self.targetAccValueQLineEdit.returnPressed.connect  (self.edit_targetAccValue)
        targetAccValue_validator = PyQt5.QtGui.QDoubleValidator()
        # charge_margin_validator.setBottom(0)
        self.targetAccValueQLineEdit.setValidator(targetAccValue_validator)
        grid.addWidget(self.targetAccValueQLineEdit,1,5,1,1)
        
        # adding it as a frame widget allows to hide it when we want to defie the logging parameters in different way (see the self.loggingSelection)
        # using frame is kind of a workaround as the grid itself cannot be hidden
        frame = PyQt5.QtWidgets.QFrame()
        frame.setLayout(grid)
        frame.hide()
        self.grid3.addWidget(frame, 2,1, 1,3)
        self.loggingSelectionGrids.append(frame)
        
    def _initialise_HistoryCaptureMode(self):
        # this method initialises the layout for capture history mode
        
        grid = PyQt5.QtWidgets.QGridLayout()
        # self.grid3.addLayout(grid, 2,1, 1,4)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("Shots to capture: "), 1,0,1,1)
        
        self.historyCaptureShots_qLineLabel = PyQt5.QtWidgets.QLineEdit(self.tabHist)
        self.n_historyCaptureShots = 1 # keep this number low (e.g. 1) in order to avoid running out of recorded shots (this is taken care of if the user changes that in the GUI, but this initial value is not checked, so keep it low)
        self.historyCaptureShots_qLineLabel.setText( str(self.n_historyCaptureShots) )
        self.historyCaptureShots_qLineLabel.editingFinished.connect(self.edit_historyCaptureShots)
        self.historyCaptureShots_qLineLabel.returnPressed.connect  (self.edit_historyCaptureShots)
        charge_margin_validator = PyQt5.QtGui.QIntValidator()
        charge_margin_validator.setBottom(0)
        self.historyCaptureShots_qLineLabel.setValidator(charge_margin_validator)        
        grid.addWidget(self.historyCaptureShots_qLineLabel,1,1,1,1)
        
        # adding it as a frame widget allows to hide it when we want to defie the logging parameters in different way (see the self.loggingSelection)
        # using frame is kind of a workaround as the grid itself cannot be hidden
        frame = PyQt5.QtWidgets.QFrame()
        frame.setLayout(grid)
        frame.hide()
        self.grid3.addWidget(frame, 2,1, 1,3)
        self.loggingSelectionGrids.append(frame)
        
    def change_accChargeEnergy_color(self, color):
        # this funtion is to be used to indicate if charge is currently being accumulated or not
        self.AccChargeLabel.setStyleSheet(f"color: {color};")
        for k in self.accChargeStats.keys():
            self.accChargeStats[k].setStyleSheet(f"color: {color};")
        self.accEnergyLabel.setStyleSheet(f"color: {color};")
        for k in self.accEnergyStats.keys():
            self.accEnergyStats[k].setStyleSheet(f"color: {color};")
    
    def edit_chargeLogFile(self):
        def warn_PermissionError():
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Permission error")
            popup.setText(f"You do not have enough permission to write to: {newLogFile.absolute().parents[0]}")
            popup.setInformativeText("Change the location of the log file or consult with the machine operator.")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Critical)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)
            popup.exec_()
            return
        
        newLogFile = self.charge_log_file_QLineEdit.text()
        
        # if input is empty, set the output file to default
        if newLogFile.strip() == "":
            newLogFile = self._get_defaul_log_file_name(self.defaultChargeLogDir)
            self.charge_log_file_QLineEdit.setText(str(newLogFile))
        else:
            newLogFile = pathlib.Path(newLogFile)
        
        # if the input is a directory, set the name of the output file to default
        if newLogFile.is_dir():
            newLogFile = self._get_defaul_log_file_name(newLogFile)
            self.charge_log_file_QLineEdit.setText(str(newLogFile))
        
        if newLogFile == self.chargeLogFile:
            return
        
        if not os.access(newLogFile.absolute().parents[0], os.W_OK):
            warn_PermissionError()
            # reset the charge log file text box to the previous valid charge log file
            self.charge_log_file_QLineEdit.setText(str(self.chargeLogFile))
            return
        
        # if the selected log file existed before, ask if the user is sure to use this one
        if newLogFile.is_file():
            def confirmButton_clock(button):
                if button.text() == "&Yes":
                    self.chargeLogFile = newLogFile
                else:
                    self.charge_log_file_QLineEdit.setText(str(self.chargeLogFile))
                
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Log file already exists")
            popup.setText("Selected log file already exists. New log would be appended to the log file.")
            popup.setInformativeText("Are you sure want to use this log file?")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Warning)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Cancel|PyQt5.QtWidgets.QMessageBox.Yes)
            popup.buttonClicked.connect(confirmButton_clock)
            popup.exec_()
        else:    
            self.chargeLogFile = newLogFile
    
    def edit_chargeOffset(self):
        self.chargeOffset = float( self.chargeOffsetQLineEdit.text() )
        # update the charge offset in BCM handlers
        for k in self.BCMsignals.keys():
            # division by 1000 to convert to nC
            self.signalHandlers[k].setChargeOffset(self.chargeOffset/1000)
    
    def edit_marginShots(self):
        self.marginShots = int( self.charge_margin_shots.text() )
        # margin shots are defined in different logging options, this will make sure it is synchronised
        self.charge_margin_shots2.setText(str(self.marginShots))
        
    def edit_marginShots2(self):
        self.marginShots = int( self.charge_margin_shots2.text() )
        # margin shots are defined in different logging options, this will make sure it is synchronised
        self.charge_margin_shots.setText(str(self.marginShots))
        
    def edit_historyCaptureShots(self):
        shots_to_capture = int(self.historyCaptureShots_qLineLabel.text())
        
        # compute the number of recorded shots = the maximum that can be captured
        BCMsignals_max_nShots = min([self.signalHandlers[scopeName].validPoints for scopeName in self.BCMsignals.keys()])
        LASERsignals_max_nShots = min([self.signalHandlers[deviceName].validPoints for deviceName in self.LASERsignals.keys()])
        max_nShots = min([BCMsignals_max_nShots, LASERsignals_max_nShots])
        
        # make sure the history capture makes sense
        if shots_to_capture > max_nShots:
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Not enough shots to capture")
            popup.setText(f"Cannot capture {shots_to_capture}, because the maximum number of valid shots is {max_nShots}.")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Critical)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)
            popup.exec_()
            
            self.historyCaptureShots_qLineLabel.setText(str(self.n_historyCaptureShots))
            
            return
        
        self.n_historyCaptureShots = int( shots_to_capture )
        
    def edit_measureShots(self):
        self.measureShots = int( self.charge_measure_shots.text() )
        
    def get_numdisp_max(self):
        return list(self.signalHandlers.values())[0].getKeeperSamples()
    
    def edit_numdisp(self):
        if self.numdisp > self.get_numdisp_max():
            self.field_numdisp.setText( str(self.get_numdisp_max()) )
            
        self.numdisp = int( self.field_numdisp.text() )
    
    def edit_ydispBCM(self):
        self.ydispBCM = float( self.field_ydispBCM.text() )
    def edit_ydispLAS(self):
        self.ydispLAS = float( self.field_ydispLAS.text() )
    
    ## RAW TAB ##
    def buildTabRaw(self):
        vbox = PyQt5.QtWidgets.QVBoxLayout(self.tabRaw)
        #initialize plot window
        self.rawPlot = BCMPlots.BCMRawCanvas(self.BCMsignals,self.LASERsignals,self.signalHandlers,\
                                             parent=self.tabRaw, width=6,height=6, minWidth=3,minHeight=3)
        vbox.addWidget(self.rawPlot)
        
        #Build the buttons
        grid = PyQt5.QtWidgets.QGridLayout()
        vbox.addLayout(grid)
        
        grid.addWidget( PyQt5.QtWidgets.QLabel("Sensitivity:"), 0,0)

        self.sensitivityDropdown = PyQt5.QtWidgets.QComboBox(self.tabRaw)
        self.sensitivityChoices  = self.japc.getParam("CA.BCM01GAIN/Setting#enumValue_values", timingSelectorOverride="" )
        self.currentSensitivityDropdownIdx = 0
        for choice in self.sensitivityChoices:
            self.sensitivityDropdown.addItem(choice)
        self.sensitivityDropdown.currentIndexChanged.connect(self.sensitivityDropdownChange)
        grid.addWidget(self.sensitivityDropdown, 0,1)
        
        self.updateTabRaw()
        
        self.updateTabRawTimer = PyQt5.QtCore.QTimer()
        self.updateTabRawTimer.setInterval(1200)
        self.updateTabRawTimer.timeout.connect(self.updateTabRaw)
        self.updateTabRawTimer.start()

    def sensitivityDropdownChange(self,idx):
        #print (self.sensitivityDropdown.currentText())
        # avoid unnecessarily sending signal
        if idx == self.currentSensitivityDropdownIdx:
            return
        
        # check if hardware is unlocked
        if self.hardwareLock.hardwareUnlocked:
            # set the new index
            self.currentSensitivityDropdownIdx = idx
            # send the signal
            self.japc.setParam("CA.BCM01GAIN/Setting#enumValue", self.sensitivityDropdown.currentText(), timingSelectorOverride="")
        else:
            # hardware is locked -> do not allow the change
            
            # reset the selection
            self.sensitivityDropdown.setCurrentIndex(self.currentSensitivityDropdownIdx)
            
            # display the warn message about the hardware lock
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Hardware locked!")
            popup.setText("Sensitivity can not be changed because the hardware lock is ON.")
            popup.setInformativeText("Unlock the hardware first.")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Critical)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)
            popup.exec_()
            return
            

    @PyQt5.QtCore.pyqtSlot()
    def updateTabRaw(self):
        "Run on a slow read loop to update the RAW tab non-plot elements when something changes"
        
        sensitivitySelected_current = self.sensitivityDropdown.currentText()
        sensitivitySelected_new     = self.japc.getParam("CA.BCM01GAIN/Setting#enumValue", timingSelectorOverride="" )
        if sensitivitySelected_current != sensitivitySelected_new:
            idx = self.sensitivityDropdown.findText(sensitivitySelected_new)
            if idx >= 0:
                # we must make sure that changing the index internally will not be interupted by the hardwareLock
                # to avoid checking the hardwareLock, following line will prevent self.sensitivityDropdownChange from triggering
                self.sensitivityDropdown.blockSignals(True)
                # now the index can be safely changed
                self.sensitivityDropdown.setCurrentIndex(idx)
                # and we have to unblock the signals in the end
                self.sensitivityDropdown.blockSignals(False)
            else:
                print ("Wut? Did not find sensitivity, idx = ", idx)
    
    ## CORRELATIONS TAB ##
    def buildTabCorr(self):
        vbox = PyQt5.QtWidgets.QVBoxLayout(self.tabCorr)
        
        #Initialize plot windows
        plotsGrid = PyQt5.QtWidgets.QGridLayout()
        vbox.addLayout(plotsGrid)
        
        #Correlations of downstream charge VS gun charge
        self.corrPlots = {}
        i=-1
        for k in self.BCMsignals.keys():
            i += 1
            if i == 0:
                continue
            self.corrPlots[k] = BCMPlots.BCMCorrelationCanvas(self.signalHandlers[list(self.BCMsignals.keys())[0]],\
                                                              self.signalHandlers[k], \
                                                              xLabel="Gun charge [nC]", \
                                                              yLabel=self.BCMsignals[k]+" charge [nC]",\
                                                              parent=self.tabCorr)
            plotsGrid.addWidget(self.corrPlots[k], 0,i-1, 1,1)            
            
        #Correlations of gun charge VS laser power
        #assert len(self.LASERsignals) == 1
                  
        self.corrPlotLASER1 = BCMPlots.BCMCorrelationCanvas(self.signalHandlers[list(self.BCMsignals.keys()  )[0]], \
                                                            self.signalHandlers[list(self.LASERsignals.keys())[1]], \
                                                            xLabel="Gun charge [nC]", yLabel="Laser energy (CLEX) [μJ]",\
                                                            parent=self.tabCorr, offset=1)
        plotsGrid.addWidget(self.corrPlotLASER1, 1,0, 1,i//2)

        self.corrPlotLASER2 = BCMPlots.BCMCorrelationCanvas(self.signalHandlers[list(self.BCMsignals.keys()  )[0]], \
                                                            self.signalHandlers[list(self.LASERsignals.keys())[0]], \
                                                            xLabel="Gun charge [nC]", yLabel="Laser energy (Laser lab) [μJ]",\
                                                            parent=self.tabCorr, offset=1)
        plotsGrid.addWidget(self.corrPlotLASER2, 1,i//2, 1,i-i//2)
        
        #Buttons to set offsets
        
        #Buttons to pause and save-to-file
    
    ## DATA DISTRIBUTION ##
    def BCMsignalCallback(self, scopeName,signal,timeAxis):
        signalName    = self.BCMsignals     [scopeName]

        #History & pyQt labels
        if self.tabsWidget.currentIndex() == self.tabIndexHist:
            self.histPlot.update_figure(scopeName, signalName, self.numdisp, self.ydispBCM )
            self.update_BCMlabels_signal.emit(scopeName)

        #Raw
        if self.tabsWidget.currentIndex() == self.tabIndexRaw:
            self.rawPlot.update_figure(signalName,signal,timeAxis)
        
        #Correlations
        if self.tabsWidget.currentIndex() == self.tabIndexCorr:
            if scopeName == list(self.BCMsignals.keys())[0]:
                #Gun -> update all
                for cplot in self.corrPlots.values():
                    cplot.update_figure(self.numdisp)
                self.corrPlotLASER1.update_figure(self.numdisp)
                self.corrPlotLASER2.update_figure(self.numdisp)
        
    def LASERsignalCallback(self, deviceName, signal=None,timeAxis=None):
        signalName    = self.LASERsignals  [deviceName]

        #History plot
        if self.tabsWidget.currentIndex() == self.tabIndexHist:
            self.histPlot.update_figure(deviceName, signalName, self.numdisp, self.ydispLAS )
            self.update_LASERlabels_signal.emit(deviceName)
        
        #Raw
        if self.tabsWidget.currentIndex() == self.tabIndexRaw and deviceName.startswith("CE.SCOPE"):
            self.rawPlot.update_figure(signalName,signal,timeAxis)

        #Correlations
        #self.corrPlotLASER.update_figure() #Done by BCMsignalCallback() when gun signal is recieved
    
    update_BCMlabels_signal = PyQt5.QtCore.pyqtSignal(str)
    @PyQt5.QtCore.pyqtSlot(str)
    def update_BCMlabels(self,scopeName):
        
        usePoints = self.numdisp
        if self.signalHandlers[scopeName].validPoints < self.numdisp:
            usePoints = self.signalHandlers[scopeName].validPoints
        timeseries = self.signalHandlers[scopeName].timeSeries[-usePoints:]
        currentCharge_nC = timeseries[-1]
        if timeseries[-1] < 0.5:
            unit_scaling = 1e3
            unit = "pC"
        else:
            unit_scaling = 1.0; 
            unit = "nC"
            
        # charge logging
        if self.chargeLogRunning:
            timeStamp = self.signalHandlers[scopeName].timeStamps[-1]
            # log the charge
            self.chargeLogger.log(self.BCMsignals[scopeName], currentCharge_nC, "nC", timeStamp)
            # update accumulated charge display
            self.accChargeStats[scopeName].setText(str(round(self.chargeLogger.getAccumulatedValue(self.BCMsignals[scopeName])[-1], 4)))
            # make sure the number of finished shots is updated
            self._updateChargeLoggerStatus()
            if self.chargeLogger.finished:
                # if charge logger finished, switch the button for next start
                self.chargeLogButton_click()

        #Current charge
        self.currentChargeWidget[scopeName].setText("{:.1f} {:}".format(currentCharge_nC*unit_scaling,unit))
      
        #Ratios
        if scopeName == list(self.BCMsignals.keys())[0]:
            self.guncharge = currentCharge_nC
        else:
            if self.guncharge == 0.0:
                self.currentRatioWidget[scopeName].setText("- %")
            else:
                self.currentRatioWidget[scopeName].setText("{:.1f} %".format(currentCharge_nC/self.guncharge*100.0))
        
        #Stats
        mean = np.average(timeseries)*unit_scaling
        RMS  = np.sqrt(np.var(timeseries))*unit_scaling
        PPvalue = (np.max(timeseries)-np.min(timeseries))*unit_scaling
        self.chargeStatsWidget[scopeName].setText("{:.3f} ± {:.3f} ({:.2f})".format(mean,RMS, PPvalue))
        if mean != 0:
            self.chargeStats2Widget[scopeName].setText("{:.2f}, {:.2f}".format(RMS/mean, PPvalue/mean))
        else:
            self.chargeStats2Widget[scopeName].setText("{:.2f}, {:.2f}".format(np.nan, np.nan))
      
    
    update_LASERlabels_signal = PyQt5.QtCore.pyqtSignal(str)
    @PyQt5.QtCore.pyqtSlot(str)
    def update_LASERlabels(self,deviceName):
        usePoints = self.numdisp
        if self.signalHandlers[deviceName].validPoints < self.numdisp:
            usePoints = self.signalHandlers[deviceName].validPoints
        timeseries = self.signalHandlers[deviceName].timeSeries[-usePoints:]

        currentEnergy = timeseries[-1] #[uJ]
        if currentEnergy < 0.5:
            unit_scaling = 1e3
            unit = "nJ"
        else:
            unit_scaling = 1.0
            unit         = "μJ"
            
        # energy logging
        if self.chargeLogRunning:
            timeStamp = self.signalHandlers[deviceName].timeStamps[-1]
            # log the energy
            self.chargeLogger.log(self.LASERsignals[deviceName], currentEnergy, "uJ", timeStamp)
            # update accumulated charge display
            self.accEnergyStats[deviceName].setText(str(round(self.chargeLogger.getAccumulatedValue(self.LASERsignals[deviceName])[-1], 4)))
            # make sure the number of finished shots is updated
            self._updateChargeLoggerStatus()
            if self.chargeLogger.finished:
                # if charge logger finished, switch the button for next start
                self.chargeLogButton_click()

        self.currentEnergyWidget[deviceName].setText("{:.1f} {:}".format(currentEnergy*unit_scaling,unit))
    
        #Stats
        mean = np.average(timeseries)*unit_scaling
        RMS  = np.sqrt(np.var(timeseries))*unit_scaling
        PPvalue = ((np.max(timeseries)-np.min(timeseries)))*unit_scaling
        
        self.energyStatsWidget[deviceName].setText("{:.3f} ± {:.3f} ({:.2f})".format(mean,RMS, PPvalue))
        if mean != 0:
            self.energyStats2Widget[deviceName].setText("{:.2f}, {:.2f}".format(RMS/mean, PPvalue/mean))
        else:
            self.energyStats2Widget[deviceName].setText("{:.2f}, {:.2f}".format(np.nan, np.nan))
    
        #Quantuum efficiency
        hPlanck     = 6.62607015e-34 # [Js]
        cLight      = 2.99792458e8   # [m/s]
        lambdaLaser = 1047.0/4.0 # [nm]
        eCharge     = 1.60217662e-19 #[C]
        if currentEnergy > 0:
            qEff = (self.guncharge * hPlanck * cLight) / (currentEnergy*1e-6 * lambdaLaser * eCharge) * 100 #[%]
            self.qEffWidget[deviceName].setText("{:.3f}".format(qEff))
        else:
            self.qEffWidget[deviceName].setText("NaN")
                
    def pauseButton_click(self):
        if self.isPaused:
            self.isPaused = False
            self.pauseButton.setText("Pause acquisition")
            self.pauseButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPause))
            for handler in self.signalHandlers.values():
                handler.pauseOff()
            
        else:
            self.isPaused = True
            self.pauseButton.setText("Continue acquisition")
            self.pauseButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPlay))
            for handler in self.signalHandlers.values():
                handler.pauseOn()
    
    def _updateChargeLoggerStatus(self):
        """This method handles (creates and updates) the logging status bar self.loggingStatusBarDynamic.
        """
        if not self.chargeLogRunning and not hasattr(self, 'loggingStatusBarDynamic'):
            return 
        
        # change the color to grey to indicate that logging is finished
        if not self.chargeLogRunning and hasattr(self, 'loggingStatusBarDynamic'):
            for i in range(len(self.loggingStatusBarDynamic)):
                self.loggingStatusBarDynamic[i].setStyleSheet("color: grey;")
            return 
        
        # make sure the widgets are created
        if not hasattr(self, 'loggingStatusBarDynamic') and self.chargeLogRunning:
            self.loggingStatusBarDynamic = [PyQt5.QtWidgets.QLabel("") for i in range(4)]
            
            self.loggingStatusBarDynamic[0].setText("Log saved to: ")
            
            self.loggingStatusBarDynamic[1] = PyQt5.QtWidgets.QLineEdit()
            self.loggingStatusBarDynamic[1].setText( str(pathlib.Path(self.chargeLogFile).resolve()) )
            self.loggingStatusBarDynamic[1].setReadOnly(True)
            
            self.loggingStatusBarDynamic[2].setText("Shots logged: ")
            if self.currentLoggingSelection == self.LoggingSelectionOptions.DefinedRuns:
                # indefinite run
                if self.measureShots == 0:
                    self.loggingStatusBarDynamic[3].setText(f" 0 ")
                # finite run
                else:
                    self.loggingStatusBarDynamic[3].setText(f" 0 / {2*self.marginShots+self.measureShots} ")
            else:
                self.loggingStatusBarDynamic[3].setText(f" 0 ")
                
            for i in range(len(self.loggingStatusBarDynamic)):
                self.status_bar.addWidget(self.loggingStatusBarDynamic[i])
            
        # obtain the current number of finished shots from the logger
        currentFinishedShots = self.chargeLogger.getNumberOfFinishedShots()
        
        # if the finished shots number did not change, do not change anything
        if hasattr(self, 'loggerFinishedShots'):
            # the second part of the if statement is to prevent the function to return if Capture history shots is used multiple times with the same number of shots to capture
            if self.loggerFinishedShots == currentFinishedShots and self.currentLoggingSelection != self.LoggingSelectionOptions.HistoryCapture:
                return
        # if it did change, update the class variable
        self.loggerFinishedShots = currentFinishedShots
        
        # this make sure that the color of labels is correct when logging is restarted and correct logging file is displayed
        if self.loggerFinishedShots == 0:
            for i in range(len(self.loggingStatusBarDynamic)):
                if self.darkModeToggled:
                    self.loggingStatusBarDynamic[i].setStyleSheet("color: white;")
                else:
                    self.loggingStatusBarDynamic[i].setStyleSheet("color: black;")
            self.loggingStatusBarDynamic[1].setText( str(pathlib.Path(self.chargeLogFile).resolve()) )
            
        # in history capture mode, there is never self.loggerFinishedShots == 1 (above) -> the log file must be specifically updated in this case
        if self.currentLoggingSelection == self.LoggingSelectionOptions.HistoryCapture:
            self.loggingStatusBarDynamic[1].setText( str(pathlib.Path(self.chargeLogFile).resolve()) )
        
        # if the logger is running, display a message depending on whether there is a specified number of shots
        if self.currentLoggingSelection == self.LoggingSelectionOptions.DefinedRuns:
            # indefinite run
            if self.measureShots == 0: 
                self.loggingStatusBarDynamic[3].setText(f" {self.loggerFinishedShots} ")
            #finite run
            else:
                self.loggingStatusBarDynamic[3].setText(f" {self.loggerFinishedShots} / {2*self.marginShots+self.measureShots} ")
        else:
            self.loggingStatusBarDynamic[3].setText(f" {self.loggerFinishedShots} ")
    
    def fileBrowserButton_click(self):
        dialog = PyQt5.QtWidgets.QFileDialog(self)
        # set the default directory as the parent directory of the actual log file
        dialog.setDirectory(str(self.chargeLogFile.absolute().parents[0]))
        dialog.setFileMode(PyQt5.QtWidgets.QFileDialog.FileMode.AnyFile)
        dialog.setNameFilter("Text or csv files (*.txt *.csv)")
        
        if dialog.exec():
            filename = dialog.selectedFiles()[0]
            self.charge_log_file_QLineEdit.setText( str(pathlib.Path(filename).resolve()) )
            self.edit_chargeLogFile()
    
    
    def chargeLogButton_click(self):
        if self.chargeLogRunning:
            self.chargeLogRunning = False
            self.chargeLogger.finish()
            # prevent changing the button for mode history capture mode
            if self.currentLoggingSelection != self.LoggingSelectionOptions.HistoryCapture:
                self.chargeLogButton.setText("Start/continue logging")
                self.chargeLogButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPlay))
            self._updateChargeLoggerStatus()
            self.change_accChargeEnergy_color("grey")
            
            # if necessary, update the log file name
            if self.CheckBox_autoUpdateLogFileName.isChecked():
                self.chargeLogFile = self._get_defaul_log_file_name(pathlib.Path(self.chargeLogFile).parents[0])
                self.charge_log_file_QLineEdit.setText( str(self.chargeLogFile) )
                
            # update status bar
            self.loggingStatusBarLabel.setText(" Logging OFF ")
            # keep the color alway black, important in dark mode
            self.loggingStatusBarLabel.setStyleSheet("color: black; background-color: lightgrey; border: 1px solid black;")
            
            
            # make sure logging set up is editable again 
            self.charge_log_file_QLineEdit.setReadOnly(False)
            self.charge_margin_shots.setReadOnly(False)
            self.charge_measure_shots.setReadOnly(False)
            
        else:
            self.chargeLogRunning = True
            
            # prevent changing the button for mode history capture mode
            if self.currentLoggingSelection != self.LoggingSelectionOptions.HistoryCapture:
                self.chargeLogButton.setText("Stop logging")
                self.chargeLogButton.setIcon(self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_MediaPause))
                
                # update status bar
                self.loggingStatusBarLabel.setText(" Logging ON ")
                # keep the color alway black, important in dark mode
                self.loggingStatusBarLabel.setStyleSheet("color: black; background-color: yellow; border: 1px solid black;")
                
            # create new charge logger
            self.chargeLogger = chargeLogTool.ChargeLogger(self.chargeLogFile)
            # add devices to monitor
            for signalName in self.BCMsignals.values():
                self.chargeLogger.addSignal(signalName)
            for signalName in self.LASERsignals.values():
                self.chargeLogger.addSignal(signalName)
            
            # this initialises the correct mode of the logger  
            if self.currentLoggingSelection == self.LoggingSelectionOptions.DefinedRuns:
                self.chargeLogger.modeMarginMeasureMarginShot(self.marginShots, self.measureShots)
            elif self.currentLoggingSelection == self.LoggingSelectionOptions.Accumulating:
                self.chargeLogger.modeTargetAccumulatedValue(self.targetValueLogPrimaryInstrument, self.targetAccValue, self.marginShots)
            elif self.currentLoggingSelection == self.LoggingSelectionOptions.HistoryCapture:
                self.chargeLogger.modeCaptureHistory(
                    self.BCMsignals, 
                    self.LASERsignals,
                    self.signalHandlers,
                    self.n_historyCaptureShots
                )
                
                # in this mode, the logging is done already in the method modeCapture history    
                # make sure the status bar is updated correctly, as it will not update the same way as in online mode
                self._updateChargeLoggerStatus()
                # this turns logging off again, as everything needed is already logged in this mode 
                self.chargeLogButton_click()
               
                popup = PyQt5.QtWidgets.QMessageBox()
                popup.setWindowTitle("Shots logged")
                popup.setText(f"Succesfully logged {self.n_historyCaptureShots} to {str(pathlib.Path(self.chargeLogFile).resolve())}.")
                popup.setIcon(PyQt5.QtWidgets.QMessageBox.Information)
                popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)
                popup.exec_()
                
                return
                
            else:
                raise NotImplementedError(f"Trying to set up the Charge and  Laser energy logger with the set up version of {self.currentLoggingSelection}, which is not defined")

            self._updateChargeLoggerStatus()
            # change to color to active based on based on darkmode
            if self.darkModeToggled:
                self.change_accChargeEnergy_color("white")
            else:
                self.change_accChargeEnergy_color("black")
            
            # make sure logging set up is uneditable 
            self.charge_log_file_QLineEdit.setReadOnly(True)
            self.charge_margin_shots.setReadOnly(True)
            self.charge_measure_shots.setReadOnly(True)
            
    def updateHardwareStatusBar(self) -> None:
        """This method updates the hardware lock status bar widget (it does not modify the lock/unlock setting itself!). """
        # make sure the self._prevHardwareUnlockedSetting exists
        if not hasattr(self, "_prevHardwareUnlockedSetting"):
            # this variable holds the previous state of the hardwareUnlocked
            # it is meant to prevent unnecessary changes to the status label AND NOT TO BE USED ANYWHERE ELSE APART FROM THIS METHOD
            self._prevHardwareUnlockedSetting = self.hardwareLock.hardwareUnlocked
            
        # check if the widget needs to be updated
        if self._prevHardwareUnlockedSetting != self.hardwareLock.hardwareUnlocked:
            # update the internal GUI variable
            self._prevHardwareUnlockedSetting = self.hardwareLock.hardwareUnlocked
            # update the status bar
            if self.hardwareLock.hardwareUnlocked:
                self.hardwareStatusBarLabel.setText(" Hardware UNLOCKED ")
                self.hardwareStatusBarLabel.setStyleSheet("color: black; background-color: red; border: 1px solid black;")
            else:
                self.hardwareStatusBarLabel.setText(" Hardware LOCKED ")
                self.hardwareStatusBarLabel.setStyleSheet("color: black; background-color: lightgreen; border: 1px solid black;")
                
    
    def hardwareLockButton_click(self):
        if self.hardwareLock.hardwareUnlocked:
            self.hardwareLock.hardwareUnlocked = False
            self.hardwareLockButton.setText("Unlock hardware")
            self.hardwareLockButton.setStyleSheet("color: black; background-color : yellow")
            
        else:
            def hardwareUnlockConfirm_click(button):
                if button.text() == "&Yes":
                    self.hardwareLock.hardwareUnlocked = True
                    self.hardwareLockButton.setText("Lock hardware")       
                    self.hardwareLockButton.setStyleSheet("color: black; background-color : red")
                    
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Unlock hardware")
            popup.setText("Unlocking hardware will allow the logging tool to perform hardware changes in the machine. Before confirming this, please talk to the operator first!")
            popup.setInformativeText("Are you sure you want to unlock hardware?")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Warning)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Cancel|PyQt5.QtWidgets.QMessageBox.Yes)
            popup.buttonClicked.connect(hardwareUnlockConfirm_click)
            popup.exec_()
    
    def _updateShutterStatus(self):
        """This method synchronises the shutter status widget with the real state."""
        if self.BeamBlocker.CheckIfShutterClosed() and self.shutterStatusBarLabel.text() != " Shutter CLOSED ":
            self.shutterStatusBarLabel.setText(" Shutter CLOSED ")
            # keep the text color alway black, important in dark mode
            self.shutterStatusBarLabel.setStyleSheet("background-color: red; border: 1px solid black; color: black;")
            return
        
        if self.BeamBlocker.CheckIfShutterOpened() and self.shutterStatusBarLabel.text() != " Shutter OPEN ":
            self.shutterStatusBarLabel.setText(" Shutter OPEN ")
            # keep the text color alway black, important in dark mode
            self.shutterStatusBarLabel.setStyleSheet("background-color: lightgreen; border: 1px solid black; color: black;")
            return

    def _updateScreenStatus(self):
        """This method synchronises the screen status widget with the real state."""
        if self.BeamBlocker.CheckIfScreenIn() and self.screenStatusBarLabel.text() != " Screen IN ":
            self.screenStatusBarLabel.setText(" Screen IN ")
            # keep the text color alway black, important in dark mode
            self.screenStatusBarLabel.setStyleSheet(f"background-color: red; border: 1px solid black; color: black;")
            return
            
        if self.BeamBlocker.CheckIfScreenOut() and self.screenStatusBarLabel.text() != " Screen OUT ":
            self.screenStatusBarLabel.setText(" Screen OUT ")
            # keep the text color alway black, important in dark mode
            self.screenStatusBarLabel.setStyleSheet(f"background-color: lightgreen; border: 1px solid black; color: black;")
            return
    
    def beamBlockButtonClick(self):
        def lockedHardwareWarning():
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Hardware locked")
            popup.setText("Beam cannot be blocked, because the hardware is locked.")
            popup.setInformativeText("Unlock the hardware first.")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Critical)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)
            popup.exec_()
            return
        
        if not self.hardwareLock.hardwareUnlocked:
            lockedHardwareWarning()
            return
        
        # perform a hardware action depending on the self.beamBlockingSelection
        # make sure that the *BySelection methods are used 
        if self.BeamBlocker.CheckIfBeamBlockedBySelection():
            self.BeamBlocker.UnblockBeamBySelection()
        else:
            self.BeamBlocker.BlockBeamBySelection()
            
        # text in the button will be changed automatically thanks to subsctiption
    
    def resetBeamBlockButton(self):
        """Resets the beam block button text to the text corresponding to the self.beamBlockingSelection and current status. This method does not do any changes to the hardware."""
        
        if self.beamBlockingSelection.currentIndex() == self.beamBlockingSelectionOptions.ScreenAndShutter.value:
            if self.BeamBlocker.CheckIfBeamUnblocked():
                self.beamBlockButton.setText("Block beam")
            else:
                self.beamBlockButton.setText("Unblock beam")
                      
        if self.beamBlockingSelection.currentIndex() == self.beamBlockingSelectionOptions.ScreenOnly.value:
            if self.BeamBlocker.CheckIfScreenOut():
                self.beamBlockButton.setText("Insert screen")
            else:
                self.beamBlockButton.setText("Retract screen")
                
        if self.beamBlockingSelection.currentIndex() == self.beamBlockingSelectionOptions.ShutterOnly.value:
            if self.BeamBlocker.CheckIfShutterOpened():
                self.beamBlockButton.setText("Close shutter")
            else:
                self.beamBlockButton.setText("Open shutter")
        
        # keep the color alway black, important in dark mode
        self.beamBlockButton.setStyleSheet(f"color: black; background-color : yellow")
    
    def loggingSelection_change(self, i):
        # if the selection is the same, do nothing 
        # this part of code must stay here in order to prevent cycling when user tries to change the selection during ongoing logging, see the next part of this function
        if self.currentLoggingSelection == self.LoggingSelectionOptions(i):
            return
        
        # prevent user to change the setting during ongoing logging
        if self.chargeLogRunning:
            # resets the selection
            self.loggingSelection.setCurrentIndex(self.currentLoggingSelection.value)
            
            popup = PyQt5.QtWidgets.QMessageBox()
            popup.setWindowTitle("Cannot change logging options")
            popup.setText("Logging options cannot be changed while the logger is running. Stop the logger and try again.")
            popup.setIcon(PyQt5.QtWidgets.QMessageBox.Critical)
            popup.setStandardButtons(PyQt5.QtWidgets.QMessageBox.Ok)
            popup.exec_()
            return

        # hide the current selection menu
        self.loggingSelectionGrids[self.currentLoggingSelection.value].hide()
        # show new selection menu
        self.loggingSelectionGrids[i].show()
        # save the current selection
        self.currentLoggingSelection = self.LoggingSelectionOptions(i)
        
    def beamBlockingSelection_change(self, i):
        # update the beam block button to show corresponging blocking option
        self.resetBeamBlockButton()
        # update the settings in the beam blocker
        # True -> instrument will be used, False -> instrument will not be used
        # this affects only the methods BeamBlocker.BlockBeamBySelection and BeamBlocker.UnblockBeamBySelection
        if self.beamBlockingSelection.currentIndex() == self.beamBlockingSelectionOptions.ScreenAndShutter.value:
            self.BeamBlocker.blockSelection["Screen"] = True
            self.BeamBlocker.blockSelection["Shutter"] = True
                
        if self.beamBlockingSelection.currentIndex() == self.beamBlockingSelectionOptions.ScreenOnly.value:
            self.BeamBlocker.blockSelection["Screen"] = True
            self.BeamBlocker.blockSelection["Shutter"] = False
                
        if self.beamBlockingSelection.currentIndex() == self.beamBlockingSelectionOptions.ShutterOnly.value:
            self.BeamBlocker.blockSelection["Screen"] = False
            self.BeamBlocker.blockSelection["Shutter"] = True
         
        
    def instrumentSelection_change(self, i):
        self.targetValueLogPrimaryInstrument = self.instrumentSelection.currentText()
        
        # make sure that correct units are displayed
        if self.targetValueLogPrimaryInstrument in self.BCMsignals.values():
            self.targetAccumulatedValueLabel.setText("Target accumulated charge [nC]: ")
        else:
            self.targetAccumulatedValueLabel.setText("Target accumulated energy [μJ]: ")
            
    def edit_targetAccValue(self):
        self.targetAccValue = float( self.targetAccValueQLineEdit.text() )
            
    def tracesVisibility_refresh(self) -> None:
        """Method to update the silenced traces in the plot based on the checkboxes.
        """
        # iterate over all checkboxes
        for k in self.tracesCheckBoxes.keys():
            if self.tracesCheckBoxes[k].isChecked():
                # if checked, make sure the trace will be visible 
                self.histPlot.setTraceVisibility(k, True)
            else:
                # otherwise silence it
                self.histPlot.setTraceVisibility(k, False)
                
    def __del__(self):
        #Stop JAPC
        try:
            self.japc.stopSubscriptions()
        except AttributeError as e:
            print()
            print("Error :", e)
            print("In BCMGui::__del__(); BCMgui probably crashed on startup, this is not the real problem...")


    def _createContextMenu(self) -> None:
        """Method to create the context (right click) contex menu."""
        self.tabHist.setContextMenuPolicy(PyQt5.QtCore.Qt.ActionsContextMenu)
        self.tabHist.addAction(self.darkModeAction)
        
        self.tabRaw.setContextMenuPolicy(PyQt5.QtCore.Qt.ActionsContextMenu)
        self.tabRaw.addAction(self.darkModeAction)
        
        self.tabCorr.setContextMenuPolicy(PyQt5.QtCore.Qt.ActionsContextMenu)
        self.tabCorr.addAction(self.darkModeAction)

    def _buildDarkModeAction(self):
        """Method to create the dark mode switch action."""
        if hasattr(self, "darkModeAction"):
            return
        
        self.darkModeAction = PyQt5.QtWidgets.QAction(self)
        self.darkModeAction.setText("Switch dark mode")
        self.darkModeAction.triggered.connect(self.switchDarkMode)
        
    def switchDarkMode(self) -> None:
        """Method for switching the dark and light mode."""
        if self.darkModeToggled == True:
            self.useLightMode()
        else:
            self.useDarkMode()
    
    def useDarkMode(self) -> None:
        """Method to turn the gui to the dark mode."""
        self.darkModeToggled = True
        
        # turn the plots to dark mode
        self.histPlot.useDarkMode()
        self.rawPlot.useDarkMode()
        for k,v in self.corrPlots.items():
            v.useDarkMode()
        self.corrPlotLASER1.useDarkMode()
        self.corrPlotLASER2.useDarkMode()

        # change the color of charge log elements so it is well visible (the color is changing when the log starts/finishes)
        if self.chargeLogRunning:
            self.change_accChargeEnergy_color("white")

        # Force the style to be the same on all OSs:
        self.qApp.setStyle("Fusion")
        
        # Update the gui itself
        # inspired by https://stackoverflow.com/questions/48256772/dark-theme-for-qt-widgets
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(53, 53, 53))
        palette.setColor(QPalette.WindowText, Qt.white)
        palette.setColor(QPalette.Base, QColor(35, 35, 35))
        palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        palette.setColor(QPalette.ToolTipBase, Qt.black)
        palette.setColor(QPalette.ToolTipText, Qt.white)
        palette.setColor(QPalette.Text, Qt.white)
        palette.setColor(QPalette.Button, QColor(53, 53, 53))
        palette.setColor(QPalette.ButtonText, Qt.white)
        palette.setColor(QPalette.BrightText, Qt.red)
        palette.setColor(QPalette.Link, QColor(42, 130, 218))
        palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        palette.setColor(QPalette.HighlightedText, Qt.black)
        self.qApp.setPalette(palette)
                    
    
    def useLightMode(self):
        """Method to turn the gui to the dark mode."""
        self.darkModeToggled = False
        
        # turn the plots to light mode
        self.histPlot.useLightMode()
        self.rawPlot.useLightMode()
        for k,v in self.corrPlots.items():
            v.useLightMode()
        self.corrPlotLASER1.useLightMode()
        self.corrPlotLASER2.useLightMode()
        
        # change the color of charge log elements so it is well visible (the color is changing when the log starts/finishes)
        if self.chargeLogRunning:
            self.change_accChargeEnergy_color("black")

        
        # reset the default color palette
        self.qApp.setPalette(self.style().standardPalette())

#Kicks off the application
if __name__ == "__main__":
    
    qApp = PyQt5.QtWidgets.QApplication(sys.argv)
    
    #Run!
    aw = BCMGui(qApp)
    aw.show()
    sys.exit(qApp.exec_())
