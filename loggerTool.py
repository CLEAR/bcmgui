import logging
import pathlib

class LoggerTool():
    def __init__(self, header=None, console_log=False):
        """Base class for logging to one or multiple files.

        Args:
            header (None | str, optional): header of the log files (set to None to ignore)
            console_log (bool, optional): _description_. Defaults to False.
        """
        self.logger = logging.getLogger("logger")
        self.logger.setLevel(logging.INFO)
        # for correct console logging
        self.logger.propagate = False
        self.log_files = {}
        self.file_existed_before = {}
                
        # THIS IS A BIT TRICKY
        # the way (I understand that) the logging module works is that the logger is created out of the scope of this script
        # and therefore, even if the LoggerTool is recreated, it may be linked to a perviously connected logging.logger
        # which already had some file handlers -> the code bellow removes the additional logger and makes sure that the
        # log is logged to the correct file
        for handler in self.logger.handlers:
            self.logger.removeHandler(handler)
        
        self.header = header
        
        # setup console logging
        if console_log:
            ch = logging.StreamHandler()
            ch.setLevel(logging.INFO)
            ch.setFormatter(logging.Formatter('%(message)s'))
            self.logger.addHandler(ch)
        
    def addLogFile(self, fileName:str)->None:
        """Method to add a file to log to.

        Args:
            fileName (str): Name / path to the file.
        """
        if fileName not in self.log_files.keys():
            # record the information if the fileName existed before
            self.file_existed_before[str(pathlib.Path(fileName).resolve())] = pathlib.Path(fileName).is_file()
            # make sure the file exists and has a header
            if not pathlib.Path(fileName).is_file():
                pathlib.Path(fileName).parents[0].mkdir(parents=True, exist_ok=True)
                if self.header is not None:
                    with open(fileName, "w") as f:
                        f.write(self.header+"\n")
            
            # add the file handler for logging
            fh = logging.FileHandler(fileName)
            self.log_files[pathlib.Path(fileName)] = fh
            fh.setLevel(logging.INFO)
            fh.setFormatter(logging.Formatter('%(message)s'))
            self.logger.addHandler(fh)
            
    def removeLogFile(self, fileName:str)->None:
        """Remove a log file from active logging.

        Args:
            fileName (str): Name / path to the file.
        """
        fileName = pathlib.Path(fileName)
        if fileName in self.log_files.keys():
            self.logger.removeHandler(self.log_files[fileName])
            self.log_files[fileName].close()
            self.log_files.pop(fileName)
            
    def log(self, msg:str)->None:
        """Method to trigger the log.

        Args:
            msg (str): Message to be logged.
        """
        self.logger.info(msg)
        # makes sure the log is written to the file
        for fh in self.log_files.values():
            fh.flush()