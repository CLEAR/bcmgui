__version__ = "1.2"

import subprocess

class Versioner(object):
    def getVersion(self):
        return __version__
    
    def isGitReporsitory(self):
        return self.getGitStatusOutput() is not None
    
    def getGitStatusOutput(self):
        try:
            gitStatus = subprocess.run(["git", "status"],stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)
            return gitStatus.stdout
        except subprocess.CalledProcessError:
            # not a git repo
            return None
    
    def reporsitoryAdjusted(self):
        gitStatusOutput = self.getGitStatusOutput()
        if gitStatusOutput is None:
            # not a git reporsitory
            return False
        
        if "Changes not staged for commit:" in gitStatusOutput:
            return True
        if "Untracked files:" in gitStatusOutput:
            return True
        if "Your branch is ahead of" in gitStatusOutput:
            return True
        if "Changes to be committed:" in gitStatusOutput:
            return True
        
        return False
    
    def checkForUpdates(self):
        "Returns true if update available."
        gitStatusOutput = self.getGitStatusOutput()
        if gitStatusOutput is None:
            # not a git reporsitory
            return False
        
        if "Your branch is behind" in gitStatusOutput:
            return True
        else:
            return False
        
    def getBrancheName(self):
        if not self.isGitReporsitory():
            return None
        git = subprocess.run(["git", "rev-parse", "--abbrev-ref", "HEAD"],stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)
        return git.stdout[:-1] # last char is \n -> we dont want it
        
        
        
        
        