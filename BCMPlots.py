# -*- coding: utf-8 -*-

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.backends.backend_qt5agg

import numpy as np

import PyQt5.QtWidgets

import datetime
import time
import pytz

import BCMSignals
import LASSignals
import timestampSynchro

import threading

def is_dst() -> bool:
    """Checks whether to use the summer time.
    
    Returns:
        bool: True if it is the daylight saving zone (-> use summer time), else False
    """
    timeZone = pytz.timezone("Europe/Zurich")
    aware_dt = timeZone.localize(datetime.datetime.today())
    return aware_dt.dst() != datetime.timedelta(0,0)

useSummerTime =  is_dst()

class MplCanvas(matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg):
    """
    General embeddable matplotlib canvas class.
    Implement `compute_initial_figure()` and `update_figure()` to use it.
    Based on https://matplotlib.org/examples/user_interfaces/embedding_in_qt5.html
    """

    axes = None

    lock = None

    prevDrawTime = None;
    minRedrawTimedelta = None;
    __maketight = None

    def __init__(self, parent=None, width=5,height=4, dpi=100, numAxesHor=1, minWidth=3,minHeight=3):

        #Used for safer multithreading and flicker avoidance
        self.lock = threading.Lock()
        self.minRedrawTimedelta = datetime.timedelta(seconds=0.5);
        self.__makeTight = False

        self.fig = matplotlib.figure.Figure(figsize=(width, height), dpi=dpi)
        super(MplCanvas,self).__init__(self.fig)

        self.axes = []
        if numAxesHor > 0:
            for i in range(numAxesHor):
                if i==0:
                    ax = self.fig.add_subplot(numAxesHor,1,i+1)
                else:
                    ax = self.fig.add_subplot(numAxesHor,1,i+1,sharex=self.axes[0])

                self.axes.append(ax)
        elif numAxesHor == -2:
            self.axes.append(self.fig.add_subplot(1,1,1))
            self.axes.append(self.axes[0].twinx())
        else:
            raise(ValueError('numAxesHor must be > 0 or -2'))

        self.compute_initial_figure()

        matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg.__init__(self, self.fig)
        self.setParent(parent)

        matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg.setSizePolicy(self, \
                           PyQt5.QtWidgets.QSizePolicy.Expanding, \
                           PyQt5.QtWidgets.QSizePolicy.Expanding)
        self.setMinimumWidth(int(dpi*minWidth))
        self.setMinimumHeight(int(dpi*minHeight))
        matplotlib.backends.backend_qt5agg.FigureCanvasQTAgg.updateGeometry(self)

    def compute_initial_figure(self):
        #To be implemented in inheriting class
        raise NotImplementedError()
    
    def update_figure(self):
        #To be implemented in inheriting class
        raise NotImplementedError()

    def resizeEvent(self,event):
        with self.lock:
            super().resizeEvent(event)
    
    def makeTight(self):
        self.__makeTight = True

    def requestDraw(self):
        "Rate limiting draw"
        with self.lock:
            if self.prevDrawTime is None:
                self.prevDrawTime = datetime.datetime.now()
            
            if self.__makeTight:
                self.fig.tight_layout()
                self.__makeTight = False
            now = datetime.datetime.now()
            if now-self.prevDrawTime > self.minRedrawTimedelta:
                self.draw()
                self.prevDrawTime = datetime.datetime.now()


    def changeBackgroundColor(self, color:str) -> None:
        """This method changes the bacground color of the figure.
        
        Arguments:
            color(str): background color to be set, accepts the same formats as matplotlib (verbosed, HEX, ...)
        """
        # sets background color of figure
        self.fig.set_facecolor(color)
        
        for ax in self.axes:
            # sets background color of the ax
            ax.set_facecolor(color)
            
            # sets the background color of the legend
            legend = ax.get_legend()
            # change color only if the legend exists (= is not None)
            if legend is not None:
                frame = legend.get_frame()
                frame.set_facecolor(color)
                frame.set_facecolor(color)
            
    def changePlotElementsColor(self, color):
        """This method changes the color of legend and axis label text as well as the color of the axis itself.
        
        Arguments:
            color(str): the color to be set, accepts the same formats as matplotlib (verbosed, HEX, ...)
        """
        for ax in self.axes:
            # text color of the legend
            legend = ax.get_legend()
            # change color only if the legend exists (= is not None)
            if legend is not None:
                for text in legend.get_texts():
                    text.set_color(color)
            
            # sets the color of the axis
            ax.spines['bottom'].set_color(color)
            ax.spines['top'].set_color(color)
            ax.spines['left'].set_color(color)
            ax.spines['right'].set_color(color)
            
            # sets the color of axis labels
            ax.xaxis.label.set_color(color)
            ax.yaxis.label.set_color(color)
            
            # sets the color of axis ticks
            ax.tick_params(axis='x', colors=color)
            ax.tick_params(axis='y', colors=color)
      
    def useDarkMode(self):
        """Method to turn the plots into dark mode."""
        self.useDarkTheme = True
        
        self.changeBackgroundColor("#434343")
        self.changePlotElementsColor("white")
    
    def useLightMode(self):
        """Method to turn the plots into light mode."""
        self.useDarkTheme = False
        
        # TODO: save somewhere the original light mode settings
        self.changeBackgroundColor("white")
        self.changePlotElementsColor("black")
            
            

class BCMHistCanvas(MplCanvas):
    "Matplotlib canvas which plots the history of the charge and laser energy"

    scaleOutMargin = 1.2 #When scaling, how much margin to take
    scaleInMargin  = 1.5 #When scaling in,  how much too big should the plot be before scaling in
    scaleYminDefault = [-0.025,-0.025] #Default minimum value, per-axis
    scaleYmincutoff  = [-0.5,-5.0] #Minimum allowed minimum y-axis, per-axis

    scaleXmargin = 5.0 #How many seconds offset to accept in the x-axis

    def __init__(self, signalNames_BCM, signalNames_laser, signalHandlers, parent=None, width=5, height=4, dpi=100, minWidth=3,minHeight=3, **kwargs):

        self.bcmSignalNames   = signalNames_BCM
        self.laserSignalNames = signalNames_laser
        self.signalHandlers   = signalHandlers
        
        self.silencedTraces = []
        
        self.ymax_all = {}
        self.ymin_all = {}
    
        for sigNam in ( self.bcmSignalNames + self.laserSignalNames ):
            self.ymax_all[sigNam] = 0.0;
            self.ymin_all[sigNam] = 0.0

        assert self.scaleOutMargin > 1.0
        assert self.scaleInMargin  > 1.0
        assert self.scaleOutMargin < self.scaleInMargin
        for symdef in self.scaleYminDefault:
            assert symdef < 0.0

        super().__init__(parent,width,height,dpi,numAxesHor=-2, minWidth=minWidth,minHeight=minHeight, **kwargs)

    def compute_initial_figure(self):
        
        self.lines = {}
        for signal in self.bcmSignalNames:
            self.lines[signal] = self.axes[0].\
                                 plot([datetime.datetime.now()-datetime.timedelta(seconds=3),\
                                       datetime.datetime.now()],\
                                      [0,5], label=signal)[0]
        i = 0
        laserColors = ('r','lime','b')
        for signal in self.laserSignalNames:
            self.lines[signal] = self.axes[1].\
                                 plot([datetime.datetime.now()-datetime.timedelta(seconds=3),\
                                       datetime.datetime.now()],\
                                      [0,1e-6], label=signal, ls='-.',color=laserColors[i])[0]
            i += 1

        #self.axes[0].set_xlabel(r'Time [$\mu$s]')
        self.axes[0].set_ylabel('Train charge [nC]')
        self.axes[0].legend(loc='upper left')
        self.axes[0].ticklabel_format(axis='y',style='sci',scilimits=(-1,4))
        
        self.axes[1].set_ylabel('Laser energy [μJ]')
        self.axes[1].legend(loc='upper right')
        self.axes[1].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))
        
        self.locator = matplotlib.dates.AutoDateLocator()
        #self.locator = matplotlib.dates.MinuteLocator()
        #self.formatter = matplotlib.dates.AutoDateFormatter(self.locator)
        self.formatter = matplotlib.dates.DateFormatter("%H:%M:%S")
        self.axes[0].xaxis.set_major_formatter(self.formatter)
        plt.setp(self.axes[0].xaxis.get_majorticklabels(), rotation=45)

        self.axes[0].grid()

    def update_figure(self, deviceName, signalName,numdisp,ymax_in):
        with self.lock:
            usePoints = numdisp
            if self.signalHandlers[deviceName].validPoints < numdisp:
                usePoints = self.signalHandlers[deviceName].validPoints

            #Update plotting
            tzone = time.timezone
            if useSummerTime:
               tzone = time.altzone
            datetimes = matplotlib.dates.epoch2num(self.signalHandlers[deviceName].timeStamps[-usePoints:] - tzone)
            yData = self.signalHandlers[deviceName].timeSeries[-usePoints:]
            self.lines[signalName].set_data(datetimes,yData)
            
            # if the signalName is silenced, no need to update the plot
            if signalName in self.silencedTraces:
                # makes sure the x axis gets updated even if all tracks silenced, maybe unnecessary?
                if len(self.silencedTraces) != len(self.bcmSignalNames) + len(self.laserSignalNames):
                    return

            #X window size
            xMin_data = min(datetimes)
            xMax_data = max(datetimes)
            for handler in self.signalHandlers.values():
                if isinstance(handler,LASSignals.LasPowHandler):
                    #Skip the untriggered laser power meter
                    # when determining the x-axis range
                    # Note other skip below
                    continue

                usePoints_loc = numdisp
                if handler.validPoints < numdisp:
                    usePoints_loc = handler.validPoints
                xMin_loc = matplotlib.dates.epoch2num(handler.timeStamps[-usePoints_loc] - tzone)
                xMax_loc = matplotlib.dates.epoch2num(handler.timeStamps[-1]             - tzone)

                if xMin_loc < xMin_data:
                    xMin_data = xMin_loc
                if xMax_loc > xMax_data:
                    xMax_data = xMax_loc

            if abs(xMin_data-xMax_data) > 1e-5 and not isinstance(handler,LASSignals.LasPowHandler):
                #We have some actual data (not from the untriggered laser power meter)!

                #Should show all data,
                # accept that window is up to scaleXmargin too wide
                xMin_now = None
                xMax_now = None
                if signalName in self.bcmSignalNames:
                    (xMin_now,xMax_now) = self.axes[0].get_xlim()
                elif signalName in self.laserSignalNames:
                    (xMin_now,xMax_now) = self.axes[1].get_xlim()
                else:
                    #This should never happen...
                    raise(NotImplementedError)   

                scaleXmargin_days = self.scaleXmargin/(24.0*3600.0)
                xMax_set = xMax_now
                xMin_set = xMin_now
                if xMax_data > xMax_now or xMax_data < xMax_now - scaleXmargin_days:
                    xMax_set = xMax_data+scaleXmargin_days; #Move window scaleXmargin ahead

                if xMin_data < xMin_now or xMin_data > xMin_now + scaleXmargin_days:
                    xMin_set = xMin_data


                #If we have actually changed scalings, update
                if xMin_set != xMin_now or xMax_set != xMax_now:
                    if signalName in self.bcmSignalNames:
                        self.axes[0].set_xlim(xMin_set,xMax_set)
                    elif signalName in self.laserSignalNames:
                        self.axes[1].set_xlim(xMin_set,xMax_set)
                    else:
                        #This should never happen...
                        raise(NotImplementedError())        

            #Set a new scale of the axes if we have moved out of range
            ymax_this = max(yData)
            ymin_this = min(yData)

            self.ymax_all[signalName] = ymax_this
            self.ymin_all[signalName] = ymin_this

            def autoscale(signalNames, axId):
                setAx = False #To set or not to set...
                (ymin_ax,ymax_ax) = self.axes[axId].get_ylim()
                ymin_set = ymin_ax
                ymax_set = ymax_ax

                #Find the global min/max
                ymax_max = ymax_this
                ymin_min = ymin_this
                for signalName in signalNames:
                    if signalName in self.silencedTraces:
                        continue
                    if self.ymax_all[signalName] > ymax_max:
                        ymax_max = self.ymax_all[signalName]
                    if self.ymin_all[signalName] < ymin_min:
                        ymin_min = self.ymin_all[signalName]

                #Are we above the limit (and not forced)? If so, scale up!
                if ymax_max > ymax_ax and ymax_in == 0.0:
                    ymax_set = ymax_max*self.scaleOutMargin
                    setAx = True

                #Are the top limit too wide? Scale down.
                if ymax_max < 0.0:
                    ymax_set = 0.0
                    if ymax_ax != 0.0:
                        setAx = True
                elif ymax_max*self.scaleInMargin < ymax_ax:
                    ymax_set = ymax_max*self.scaleOutMargin
                    setAx=True

                #Apply a scaling override?
                if ymax_in > 0.0:
                    ymax_set = ymax_in
                    setAx = True

                #What about the lower limit?
                if ymin_min > self.scaleYminDefault[axId]:
                    ymin_set = self.scaleYminDefault[axId]
                    if ymin_ax != self.scaleYminDefault[axId]:
                        setAx = True
                else:
                    #Scale out!
                    if ymin_min < ymin_ax:
                        ymin_set = ymin_min*self.scaleOutMargin
                        setAx = True
                    elif ymin_min*self.scaleInMargin > ymin_ax:
                        ymin_set = ymin_min * self.scaleOutMargin
                        setAx = True

                    if ymin_set < self.scaleYmincutoff[axId]:
                        ymin_set = self.scaleYmincutoff[axId]

                if setAx:
                    self.axes[axId].set_ylim(ymin_set, ymax_set)
                    self.makeTight()

            if signalName in self.bcmSignalNames:
                autoscale(self.bcmSignalNames, 0)
            elif signalName in self.laserSignalNames:
                autoscale(self.laserSignalNames, 1)
            else:
                raise(NotImplementedError("This should never happen..."))

        self.requestDraw()
        
    def update_legends(self, signalName:str) -> None:
        """Method to update legend based on the silencedTraces.
        If and only if signalName is in the silencedTraces, it is not mentioned in the legend.

        Args:
            signalName (str): name of the signal 
        """
        # find which legend is to be updated
        if signalName in self.bcmSignalNames:
            # check if there are still some inputs for the legend, otherwise remove it
            if set(self.bcmSignalNames) <= set(self.silencedTraces):
                try:
                    self.axes[0].get_legend().remove()
                except AttributeError:
                    # in case it was already removed
                    pass
            else:
                # redraw the legend if it is save to (there are inputs for the legend)
                self.axes[0].legend(loc='upper left')
        if signalName in self.laserSignalNames:
            # check if there are still some inputs for the legend, otherwise remove it
            if set(self.laserSignalNames) <= set(self.silencedTraces):
                try:
                    self.axes[1].get_legend().remove() 
                except AttributeError:
                    # in case it was already removed
                    pass
            else:
                # redraw the legend if it is save to (there are inputs for the legend)
                self.axes[1].legend(loc='upper right')
                
    def setTraceVisibility(self, traceName:str, enable:bool) -> None:
        """Sets the visibility of the trace with traceName in the history plot.
        
        Args:
            trackName (str): name of the trace, whose visibility should be changed
            enable (bool): if True, the trace will be visible
        """
        with self.lock:
            # check if it is really needed to change something
            if traceName in self.silencedTraces and not enable:
                return 
            if traceName not in self.silencedTraces and enable:
                return 
            
            if enable:
                # remove from the list of silenced tracks
                self.silencedTraces.remove(traceName) 
                # make sure the transparency of the line is 1 and the line has correct label in the legend
                self.lines[traceName].set(alpha=1, label=traceName)
            else:
                # add to the list of silenced names
                self.silencedTraces += [traceName]
                # sets the transparency of the line to 0 and label="_nolegend_" will remove it from the legend
                self.lines[traceName].set(alpha=0, label="_nolegend_")
            # legend needs to be redrawn
            self.update_legends(traceName)
        # redraw the plot
        self.requestDraw()  

    def resizeEvent(self,event):
        self.makeTight()
        super().resizeEvent(event)


class BCMRawCanvas(MplCanvas):
    "Matplotlib canvas which plots the raw BCM signals"

    def __init__(self, BCMsignals, LASERsignals, signalHandlers, parent=None, width=5, height=4, dpi=100, minWidth=3,minHeight=3, **kwargs):
        self.BCMsignals     = BCMsignals
        self.LASERsignals   = LASERsignals
        self.signalHandlers = signalHandlers
        self.firstDraw = True
        self.firstDrawBCM = True # Used for the axvspan
        self.ymax_all = {}

        super().__init__(parent,width,height,dpi,numAxesHor=1, minWidth=minWidth,minHeight=minHeight, **kwargs)

    def compute_initial_figure(self):
        
        self.lines = {}
        for k,v in self.BCMsignals.items():
            self.lines[v] = self.axes[0].plot([0,],[0,], label=v)[0]
        for k,v in self.LASERsignals.items():
            if k.startswith("CE.SCOPE"):
                self.lines[v] = self.axes[0].plot([0,],[0,], label=v, linestyle='--')[0]
            
        self.axes[0].set_xlabel(r'Time [$\mu$s]')
        self.axes[0].set_ylabel('Signal [V]')
        
        self.axes[0].legend(loc='upper right')
        
        #self.requestDraw()

    def update_figure(self,signalName,rawData,timeAxis):
        with self.lock:
            self.lines[signalName].set_data(timeAxis/1e6,rawData)

            if self.firstDraw:
                self.axes[0].set_xlim(0,max(timeAxis)/1e6)
                self.firstDraw = False

            if self.firstDrawBCM:
                avg_start = 0
                avg_end   = 0
                for k,v in self.BCMsignals.items():
                    if not v == signalName:
                        continue
                    sh = self.signalHandlers[k]
                    if sh.avg_start != avg_start or sh.avg_end != avg_end:
                        avg_start = sh.avg_start
                        avg_end = sh.avg_end
                        self.axes[0].axvspan(timeAxis[avg_start]/1e6, timeAxis[avg_end]/1e6, alpha=0.3)
                    self.firstDrawBCM = False

            setAx = False
            ymax = max(rawData)
            ymin = min(rawData)
            self.ymax_all[signalName] = ymax

            (ymin_ax,ymax_ax) = self.axes[0].get_ylim()

            #Zoom out?
            if ymax_ax < ymax:
                ymax_set = ymax*1.1
                setAx = True
            else:
                ymax_set = ymax_ax

            if ymin_ax > ymin:
                if ymin < 0.0:
                    ymin_set = ymin*1.1
                else:
                    ymin_set = 0.0
                setAx = True
            else:
                ymin_set = ymin_ax

            #Zoom in?
            ymax_overall = 0.0
            for ym in self.ymax_all.values():
                if ym > ymax_overall:
                    ymax_overall = ym
            if ymax_overall < 0.5*ymax_ax:
                ymax_set = 1.1*ymax_overall
                setAx = True

            if setAx:
                self.axes[0].set_ylim(ymin_set, ymax_set)

            self.makeTight()
        self.requestDraw()

    def resizeEvent(self,event):
        super().resizeEvent(event)


class BCMCorrelationCanvas(MplCanvas):
    "Matplotlib canvas which plots the correlation between two signals"

    def __init__(self, signalHandlerX, signalHandlerY, \
                 xLabel="", yLabel="", \
                 parent=None, width=5, height=4, dpi=100, minWidth=3,minHeight=3,offset=0):
        
        self.signalHandlerX = signalHandlerX
        self.signalHandlerY = signalHandlerY
                
        self.xLabel = xLabel
        self.yLabel = yLabel

        self.offset = offset
        
        super().__init__(parent,width,height,dpi,numAxesHor=1, minWidth=minWidth,minHeight=minHeight)
        #self.fig.subplots_adjust(bottom=0.15, left=0.25, top=0.99, right=0.99)
        self.makeTight()

    def compute_initial_figure(self):
        self.plotObject = self.axes[0].plot([1,],[1,], ls=" ", marker='x')[0]
        self.axes[0].set_xlabel(self.xLabel)
        self.axes[0].set_ylabel(self.yLabel)
        
        self.axes[0].grid()
                

    def update_figure(self, numdisp):
        with self.lock:
            use_points = numdisp;
            if self.signalHandlerX.validPoints < use_points:
                use_points = self.signalHandlerX.validPoints
            if self.signalHandlerY.validPoints < use_points:
                use_points = self.signalHandlerY.validPoints

            xTimes = self.signalHandlerX.timeStamps
            xData  = self.signalHandlerX.timeSeries

            yTimes = self.signalHandlerY.timeStamps
            yData  = self.signalHandlerY.timeSeries

            #Trim off the pre-valid part
            xTimes = xTimes[-use_points:]
            xData  = xData[-use_points:]
            yTimes = yTimes[-use_points:]
            yData  = yData[-use_points:]

            #Data synchronization
            try:
                selectX, selectY = timestampSynchro.synchronizeTwo(xTimes,yTimes)
            except ValueError as e:
                print('Error in synchronization: ', e)
                return
            if len(selectX) < 1:
                return

            xData = xData[selectX]
            yData = yData[selectY]   

            if not (np.isnan(xData).any() or np.isnan(yData).any()):
                if self.offset != 0:
                    if self.offset > 0:
                        xData = xData[0:-self.offset-1]
                        yData = yData[self.offset:-1]
                    elif self.offset < 0:
                        yData = yData[0:self.offset-1]
                        xData = xData[-self.offset:-1]

                self.plotObject.set_data(xData,yData)

                self.axes[0].set_xlim(min(xData)*0.9, max(xData)*1.1)
                self.axes[0].set_ylim(min(yData)*0.9, max(yData)*1.1)

            self.makeTight()
        
        self.requestDraw()

    def resizeEvent(self,event):
        super().resizeEvent(event)
