# -*- coding: utf-8 -*-
"Module for synchronizing two or more datetime streams"

import numpy as np

def synchronize(timestamps):
    """
    Given a list of  numpy array of timestamps, produce a set of index arrays
    that synchronizes the data streams
    """
    raise (NotImplementedError())

def synchronizeTwo(timestampsA, timestampsB, maxSearch = 5):
    """
    Synchronizes two streams of timestamps (unix time)
    maxSearch controls how far it should search in time
    """
    
    #print(timestampsA-timestampsA[0])
    #print(timestampsB-timestampsA[0])
    
    if len(timestampsA) < 3 or len(timestampsB) < 3:
        return (np.asarray([]),np.asarray([]))
    
    #Search from the middle of the A dataset to find
    # the closest match in time from the B dataset
    iMidA = int(len(timestampsA)/2)
    iMidB = int(len(timestampsB)/2)
    
    dtMin = abs(timestampsA[iMidA] - timestampsB[iMidB])
    diMin = 0
    for i in range(maxSearch):
        if iMidB+i < len(timestampsB):
            dt = abs(timestampsA[iMidA] - timestampsB[iMidB+i])
            if dt < dtMin:
                dtMin = dt
                diMin = i
        if iMidB-i > 0:
            dt = abs(timestampsA[iMidA] - timestampsB[iMidB-i])
            if dt < dtMin:
                dtMin = dt
                diMin = -i
    #print ("Found dtMin =", dtMin, "diMin =", diMin)
    
    #Find the typical timestep.
    # Should probably get the mode and not the mean...
    dtA = np.mean(np.diff(timestampsA))
    dtB = np.mean(np.diff(timestampsB))
    dtAB = (dtA+dtB)/2
    if abs(dtA-dtB) > 0.2*dtAB:
        raise (ValueError("Timesteps of the two arrays should be the same!"))
    
    #Loop forwards from the middle to sync up
    # Code should be relatiely easy to generalize to handle skips...
    selectA1 = []
    selectB1 = []
    i = 0
    iMidA1 = iMidA
    iMidB1 = iMidB
    while True:
        if iMidA1+i >= len(timestampsA) or iMidB1+i+diMin >= len(timestampsB):
            break
        
        dt = abs(timestampsA[iMidA1+i]-timestampsB[iMidB1+i+diMin])
        if dt > dtMin*2:
            hasRecovered = False
            #Search iMidA1 forward to recover
            for j in range(1,maxSearch):
                if iMidA1+i+j >= len(timestampsA):
                    break
                dt = abs(timestampsA[iMidA1+i+j]-timestampsB[iMidB1+i+diMin])
                if dt < dtMin*2:
                    iMidA1 += j
                    hasRecovered = True
                    break
            #Search iMidB1 forward to reover
            if not hasRecovered:
                for j in range(1,maxSearch):
                    if iMidB1+i+diMin+j >= len(timestampsB):
                        break
                    dt = abs(timestampsA[iMidA1+i]-timestampsB[iMidB1+i+diMin+j])
                    if dt < dtMin*2:
                        iMidB1 += j
                        hasRecovered = True
                        break
            if not hasRecovered:
                raise ValueError("There was an unrecoverable time skip! dt={}, i={}".format(dt,i))
            
        selectA1.append(iMidA+i)
        selectB1.append(iMidB+i+diMin)
        
        i += 1
    
    #Loop backwards from the middle...
    selectA2 = []
    selectB2 = []
    i = -1
    while True:
        if iMidA+i < 0 or iMidB+i+diMin < 0:
            break
        
        dt = abs(timestampsA[iMidA+i]-timestampsB[iMidB+i+diMin])
        if dt > dtAB*0.9:
            raise ValueError("There was a time skip! dt={}, i={}".format(dt,i))
            
        selectA2.append(iMidA+i)
        selectB2.append(iMidB+i+diMin)
        
        i -= 1
    
    #Combine the arrays and return
    return(np.asarray(selectA2[::-1]+selectA1),\
           np.asarray(selectB2[::-1]+selectB1))