# BCMGUI changelog
- Merge Request !12, 2023-06-12, Zdenek Vostrel and Kyrre Sjobak
    - Replaced the deprecated `Samples` property from the BPM data acquisition with the `SamplesFromTrigger`
    - Updated one of the laser power meters

- Merge Request !10, 2023-06-06, Zdenek Vostrel and Kyrre Sjobak
    - BPM data are processed for higher (> 0.8 Hz) frequency
    - beam is now blocked after the end of logging even if the number of margin shots is set to zero
    - fixed colors of some widgets in dark mode
    - reducesd usage of japc.getParam in the beam blocking tool
    - added graphical option to add custom charge offset
    - GUI now displayes the current version and basic git information
    - made GUI more compact
    - by default, GUI now opens in full screen mode

- Merge Request !9, 2023-05-02, Zdenek Vostrel and Kyrre Sjobak
    - fixed issue that caused the BCMGui to crash when trying to block the beam

- Merge Request !8, 2023-05-02, Zdenek Vostrel and Kyrre Sjobak 
    - implemented dark mode for the BCMGui
    - dark mode can be toggled in the context menu, which pops up after right clicking anywhere in the BCMGui

- Merge Request !7, 2023-04-14, Kyrre Sjobak and Zdenek Vostrel
    - `startBCMGui.sh` starts the version of BCMGui in its directory (no longer needed to specify the BCMGui path in `startBCMGui.sh`)

- Merge Request !6, 2023-03-29, Zdenek Vostrel and Kyrre Sjobak
    - fixed summer time change issue (#1)
    - the correct time zone is selected automatically when the GUI starts -> the GUI has to be restarted

- Merge Request !5, 2023-03-20, Zdenek Vostrel and Kyrre Sjobak
    - hardware lock is automatically synchronised between the GUI and hardware control classes (fixing #13)
    - implemented hardware lock for the sensitivity selection
    - beam can now be blocked by the shutter in addition to the screen (#14)
    - added option to select what to use to block the beam (screen and/or shutter)
    - hardware lock, screen and shutter status widget are now synchronised by callback methods 
    - fixed DeprecationWarning

- Merge request !3, 2023-03-20, Zdenek Vostrel and Kyrre Sjobak
    - implemented charge and laser energy logging tool into the GUI (solved #8)
    - implemented target accumulated charge / laser energy feature
    - fixed the old logging tool `chargeLoggerTool.py` (it is preferable to use the new logging tool implemented directly in the GUI)
    - implemented hardware lock
    - for detailed overview, see presentation https://indico.cern.ch/event/1259113/

- Merge request !2, 2022-12-12, Zdenek Vostrel and Kyrre Sjobak
    - Make it possible to enable/disable showing traces in the GUI, fixing issue #4
