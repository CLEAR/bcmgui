import loggerTool
from datetime import datetime
import pyjapc
import numpy as np
import enum
import pathlib

from auxiliary import BeamBlocker

class ChargeLogger():
    def __init__(self, logFile:str):
        """Class for logging and accumulating charge and energy.

        Args:
            logFile (str): Name / path to the log file.
        """
        # internal variable containing the low levvel logger, will be initialised automatically later for correct header display
        self._logger = None
        self.logFile = pathlib.Path(logFile)
        
        # beam block control
        self.BeamBlocker = BeamBlocker.BeamBlocker()
        
        # save the number of shots to be performed
        self.marginShots  = 0
        self.measureShots = 0
        self.maxShots = -1
        
        # mode of charge logger operation
        self.modes = enum.Enum('Mode', ['Indefinite', 'DefinedRuns', 'Accumulating', "HistoryCapture"], start=0)
        # Indefinite     -> indefinite run, has to be stopped by user
        # DefinedRuns    -> runs for: marginShots + measureShots + marginShots
        # Accumulating   -> runs until accumulated value is recorded on given instrument
        # HistoryCapture -> captures n last shots
        self.currentMode = self.modes.Indefinite
        
        # dictionary for saving signalNames and their accumulated values
        self.accumulatedSignal = {}
        
        # contains the information if the logging is done (only importnant for nonzero value of self.measureShots)
        self.finished = False
        
        # the highest index of recorded shot
        self.maxShotNum = -1
        
        # for better readable files, will be adjusted later
        self.maxSignalNameLength = len(" signalName")
       
    def modeMarginMeasureMarginShot(self, marginShots:int, measureShots:int):
        """ Method to switch the logger to the margin and measure shots mode.
        
        Args:
            marginShots (int): Number of shots to be performed before and after the measurements (which is defined by measureShots).
            measureShots (int): Number of shots to be measured. Set to 0 for indefinite number of shots.
        """
        # if measure shots are set to 0, it means it is an indefinite run
        if measureShots == 0:
            self.currentMode = self.modes.Indefinite
            return
        
        self.currentMode = self.modes.DefinedRuns
        self.marginShots = marginShots
        self.measureShots = measureShots
        self.maxShots = 2*marginShots+measureShots 
        
        # makes sure the hardware is correctly configured
        self._hardwareConfiguration()
        
    def modeTargetAccumulatedValue(self, primaryInstrument:str, targetValue:float, marginShots:int):
        """ Method to switch the logger to the target value mode.
        
        Args:
            primaryInstrument (str): Name of the primary instrument, which is used to determine whether the target value was reached.
            measureShots (float): Target value of accumulated charge [nC] or energy [μJ] to be reached on the primary instrument.
            marginShots (int): Number of shots to be performed before and after the measurements (which is defined by measureShots).
        """
        self.currentMode = self.modes.Accumulating
        self.primaryInstrument = primaryInstrument
        self.targetValue = targetValue
        self.marginShots = abs(marginShots)
        self.targetValueReached = False
        self.finalMarginRunsDone = -1 # initialised to -1 as the first run when the target value is reached is still counted as the measure run
        
        # makes sure the hardware is correctly configured at the beginning
        self._hardwareConfiguration()
        
        if primaryInstrument not in self.accumulatedSignal.keys():
            raise Exception(f"The primary instrument {primaryInstrument} has not yet been initialised in the logging tool")
        
    def modeManual(self) -> None:
        """ Method to switch the logger to the manual mode.
        """
        self.currentMode = self.modes.Indefinite 
        
    def modeCaptureHistory(self, chargeSignals:dict, laserSignals:dict, signalHandlers:dict, nShots:int) -> None:
        """ Method to switch the logger to the history capture mode.
        
        Args:
            chargeSignals   (dict): dictionary containing the BCM names
            laserSignals    (dict): dictionary containing the laser names
            signalHandlers  (dict): signal handlers containing the data
        """
        self.currentMode = self.modes.HistoryCapture
        
        self.maxShots = nShots
          
        # check if there is anything t log
        if (len(chargeSignals) + len(chargeSignals))  == 0 or nShots == 0:
            self.finish()
        
        # merge the signals in order not to duplicate code
        signals = chargeSignals|laserSignals
        for signalName in signals.keys():
            # check if this signalName was initialised, otherwise skip it
            if signals[signalName] not in self.accumulatedSignal.keys():
                continue
            # get the data
            signal = signalHandlers[signalName]
            # check if there are enough valid points
            if signal.validPoints < nShots:
                continue
            # indicate if this is a laser signal in order to correctly assign the unit
            laser = signalName in laserSignals.keys()
            # log the values
            for i in range(nShots)[::-1]:
                currentCharge_nc = signal.timeSeries[-i-1]
                timeStamp = signal.timeStamps[-i-1]
                  
                if laser:
                    self.log(laserSignals[signalName], currentCharge_nc, "uJ", timeStamp)
                else:
                    self.log(chargeSignals[signalName], currentCharge_nc, "nC", timeStamp)
                
        self.finish()
    
    @property
    def logger(self):
        if self._logger is None:
            self._logger = loggerTool.LoggerTool(header=f"{'signalName':<{self.maxSignalNameLength}}, shotIdx, {'time':<{19}}, {'unixtime':<18}, {'value':<8}, {'accValue':<10}, unit")
            self._logger.addLogFile(self.logFile)
            # if the file existed before, add comment with the end of the previous run
            if self._logger.file_existed_before[str(self.logFile.resolve())]:
                self.logger.log("# end of the run")
        return self._logger
        
    def _hardwareConfiguration(self) -> None:
        """Internal method for hardware configuration. If there is any hardware configuration to be done, add it here! Nothing will be performed if hardwareUnlocked is False.
        """
        # the hardware lock is not checked here, but directly in the beam block/unblock functions for better synchronisation of the lock
        
        # no automated hardware configuration in indefinite run mode
        if self.currentMode == self.modes.Indefinite or self.currentMode == self.modes.HistoryCapture:
            return 
        
        # specific setting for mode with defined number of runs
        if self.currentMode == self.modes.DefinedRuns:
            # no automatic hardware configuration if measure shots are not specified (i.e. set o 0)
            if self.measureShots == 0:
                return
            
            # get te shot num
            if len(self.accumulatedSignal) != 0:
                shotNum = max([len(self.accumulatedSignal[signalName]) for signalName in self.accumulatedSignal.keys()]) -1
            else: 
                shotNum = 0
            
            # make sure hardware movement is not triggered unnecessary
            if shotNum == self.maxShotNum:
                return 
            self.maxShotNum = shotNum
            
            # at the start block beam 
            if shotNum == 0 and self.marginShots != 0:
                self.BeamBlocker.BlockBeamBySelection()
            
            # unblock the beam after marginShots
            if shotNum == self.marginShots:
                self.BeamBlocker.UnblockBeamBySelection()
            # block the beam again after measure shots
            elif shotNum == self.marginShots+self.measureShots:
                self.BeamBlocker.BlockBeamBySelection()
            return
            
        # specific setting for accumulating mode    
        if self.currentMode == self.modes.Accumulating:
            if self.primaryInstrument in self.accumulatedSignal.keys():
                shotNum = len(self.accumulatedSignal[self.primaryInstrument])-1
                
                # make sure hardware movement is not triggered unnecessary
                if shotNum == self.maxShotNum:
                    return 
                self.maxShotNum = shotNum
                
                # at the start block the beam
                if shotNum == 0 and self.marginShots != 0:
                    self.BeamBlocker.BlockBeamBySelection()
                
            # unblock the beam after marginShots
            if shotNum == self.marginShots:
                self.BeamBlocker.UnblockBeamBySelection()
            # block the beam again in just after the target value is reached
            elif self.targetValueReached and shotNum == self.maxShots - self.marginShots:
                self.BeamBlocker.BlockBeamBySelection()
                
            return 
    
    def addSignal(self, signalName:str) -> None:
        """Adds a signal to the subscription list.

        Args:
            signalName (str): Name of the signal to be subscribed.
        """
        self.accumulatedSignal[signalName] = [0]
        self.maxSignalNameLength = max([self.maxSignalNameLength, len(signalName)])
        
    def removeSignal(self, signalName:str) -> None:
        """Removes a signal from subcription list.

        Args:
            signalName (str): Name of the signal to be removed.
        """

        if signalName in self.accumulatedSignal.keys():
            self.accumulatedSignal.pop(signalName)
        
    def reset(self) -> None:
        """Resets the accumulated signals (sets all values to 0).
        """
        for k in self.accumulatedSignal.keys():
            self.accumulatedSignal[k] = [0]
            
    def log(self, signalName:str, value:float, unit:str, timeStamp:float) -> None:
        """Main method for the charge and energy logging. Use it in the subscription.

        Args:
            signalName (str): Specify which signal triggered the log.
            value (str | float): Charge or energy value.
            unit (str): Unit of the value provided.
            timeStamp (str | float): UNIX timestamp from the signal.
        """
        # if process finished, prevent from logging any additional values
        if self.finished:
            return
        # make sure the signal is subscribed
        if signalName not in self.accumulatedSignal.keys():
            return
        
        # make sure only given number of shots is written for each signal OR the number of shots is not limited
        # -1 (in the first condition) because the the list is initialised as [0], which does not correspond to any shot
        if (len(self.accumulatedSignal[signalName]) - 1) < self.maxShots or self.currentMode == self.modes.Indefinite or self.maxShots == -1:
            self.accumulatedSignal[signalName].append(self.accumulatedSignal[signalName][-1] + float(value))
            
            shotIdx = str(len(self.accumulatedSignal[signalName]) - 1)
            accumulatedValue = self.accumulatedSignal[signalName][-1]
            humanReadableTime = datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d %H:%M:%S')

            # log with the low level logger
            self.logger.log(f'{signalName:<{self.maxSignalNameLength}}, {shotIdx:>{len("shotIdx")}}, {humanReadableTime}, {timeStamp:<18}, {float(value):>+8.3f}, {accumulatedValue:>+10.3f}, {unit:>{len("unit")}}')
        # check the target accumulation if needed, the function does nothing if the mode is not 2
        self.performAccumulatedCheck(signalName)
        # check if there are any hardware adjustments
        self._hardwareConfiguration()
        # check if the measurement is complete
        if self.checkStopCondition():
            self.finish()
        
    def performAccumulatedCheck(self, signalName):
        """ Method to check if target value on primary instrument in target accumulated value was reached.
        
        The method sets self.targetValueReached = True if target value was reached. Also, it updates the
        number of finished shots (which is calculated as the number of shots recorded on the primary instrument).
        
        Args:
            signalName (str): Specify which signal triggered this method.
        """
        # make sure this method is active only during the correct mode
        if self.currentMode != self.modes.Accumulating:
            return
        
        # make sure this method is triggered by the primary instrument only
        if signalName != self.primaryInstrument:
            return
        
        # if target accumulated value was already reached, do not trigger the funtion again
        if self.targetValueReached:
            return
        
        # check if target value has been reached
        if self.accumulatedSignal[self.primaryInstrument][-1] >= self.targetValue:
            self.targetValueReached = True
            # if maximum shots were not specifically set, set it now
            if self.maxShots == -1:
                # maximum number of shots is the number of shots required to reach the accumulated value + the margin shots 
                # -> this will be used to record the same number of shots in the other instruments
                # -1 because the the list is initialised as [0], which does not correspond to any shot
                self.maxShots = len(self.accumulatedSignal[self.primaryInstrument]) + self.marginShots -1
        
        
    def checkStopCondition(self) -> bool:
        """Checks if the stop condition for the measurement is fulfilled.

        Returns:
            bool: Return True if the measurement is completed, otherwise False
        """
        if self.finished:
            return True
        
        # indefinite mode 
        if self.currentMode == self.modes.Indefinite:
            return False
        
        # history capture mode
        if self.currentMode == self.modes.HistoryCapture:
            return False
        
        if self.currentMode == self.modes.DefinedRuns:
            # check if number of shots from each signal is equal to the maximum number of shots
            minShotNum = self.getNumberOfFinishedShots()
            if minShotNum == self.marginShots + self.measureShots + self.marginShots:
                return True
            
        if self.currentMode == self.modes.Accumulating:
            if not self.targetValueReached:
                return False
            if self.maxShots == self.getNumberOfFinishedShots():
                return True
            
        return False
    
    def getAccumulatedValue(self, signalName:str) -> list:
        """Returns list of the accumulated values from a subscribed signal. 

        Args:
            signalName (str): Name of the signal.   

        Returns:
            list: List of accumulated values. If the signal has not been subscribed, returns [0].
        """
        if signalName in self.accumulatedSignal.keys():
            return self.accumulatedSignal[signalName]
        else:
            # if not subscribed, then accumulated values = 0 so it returns this
            return [0]
        
    def getNumberOfFinishedShots(self)->int:
        """Returns number of finished shots (finished shot means signal came from all subscribed devices).
        
        Return:
            int: number of finished shots.
        """
        if len(self.accumulatedSignal.keys()) != 0:
            # -1 because the values accumulated values are initialised with value 0, without recording any shots
            return min([len(self.accumulatedSignal[signalName]) for signalName in self.accumulatedSignal.keys()])-1
        else:
            return 0
        
    def finish(self):
        """Method to be called upon finish."""
        if self.finished:
            return 
        
        self.finished = True
        
        # this will make sure the the hardware is correctly configured after the end of the logging
        self._hardwareConfiguration()
