#! /usr/bin/env bash
# Little script to start BCMGui from e.g. CCM

source /acc/local/share/python/acc-py/setup.sh

#BCMguiPATH=/clear/data/OperationTools/bcmgui/

#From https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
BCMguiPATH=$SCRIPTPATH

echo "cd to "$BCMguiPATH" ..."
cd $BCMguiPATH
python3 BCMGui.py
