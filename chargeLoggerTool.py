#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 12:26:08 2019

This is a legacy logging tool. New version of this was implemented into the BCMGui 
and it is recommended to use that one. However, this one should also work fine.

Please note that this script perform hardware changes to the accelerator. 
Therefore, TALK TO THE OPERATORS BEFORE RUNING THIS SCRIPT. 

@author: clearop
"""
import pyjapc
japc = pyjapc.PyJapc("SCT.USER.ALL")

import matplotlib.pyplot as plt

import sys
assert len(sys.argv) == 2
fName = sys.argv[1]
#fName = input("Filname to write to:")
oFile = open(fName, 'w')

print()
print("hit ENTER to go!")
input("Acquisition will stop by itself after the programmed amount of points, or when you hit ENTER again.")

import BCMSignals
GunBCM = "CE.SCOPE61.CH01"
vesperBCM = "CE.SCOPE61.CH02"
THzBCM = "CE.SCOPE61.CH03"

startPoints = 10
numPoints  = 10
endPoints   = 10

signalHandler = None
accumulated = []
stopNow=False

def callback(scopeName,signal,timeAxis):
    shotNum = signalHandler.validPoints
    if len(accumulated) == 0:
        accumulated.append( signalHandler.timeSeries[-1] )
    else:
        accumulated.append( accumulated[-1] + signalHandler.timeSeries[-1] )
    print(scopeName,
          shotNum, \
          signalHandler.timeStamps[-1], \
          signalHandler.timeSeries[-1]*1e3,\
          accumulated[-1]*1e3)
    
    
    if shotNum == startPoints:
        japc.setParam('CA.BTV0215/Setting#screenSelect', 0, timingSelectorOverride="")
        print("screen OUT")
    if shotNum == startPoints+numPoints:
        japc.setParam('CA.BTV0215/Setting#screenSelect', 1, timingSelectorOverride="")
        print("screen IN")
    if shotNum == startPoints+numPoints+endPoints or stopNow == True:
        japc.stopSubscriptions()
        print("DONE")
        
        print("writing data to %s ..."%(fName))
        oFile.write("# shotIdx unixtime charge[pC] accumulatedCharge[pC]\n")
        for i in range(shotNum):
            oFile.write("%i %s %.2f %.2f\n"%(i+1, \
                str(signalHandler.timeStamps[-shotNum+i]), signalHandler.timeSeries[-shotNum+i]*1e3, accumulated[-shotNum+i]*1e3 ))
        oFile.close()
        print("Data written, hit enter to finish")
        

signalHandler = BCMSignals.BCMHandler(GunBCM, japc, callback)

assert (startPoints+numPoints+endPoints) < signalHandler.getKeeperSamples()

japc.startSubscriptions()

input("hit enter to exit\n")
stopNow=True

shotNum=startPoints+numPoints+endPoints
plt.plot(signalHandler.timeSeries[-shotNum:]*1e3)
plt.axvline(startPoints+1)
plt.axvline(startPoints+numPoints+1)
plt.xlabel('#shots')
plt.ylabel('Charge [pC]')
plt.show()
